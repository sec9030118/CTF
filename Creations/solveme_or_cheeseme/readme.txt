Solve me or Cheese me
--
Reverse and solve the puzzle or (and!) ignore it and grab the flag anyways.
Flag format: flag{[0-9A-Za-z_]+}
(no stupid brute-force, ~1sec is enough for each expected approach)

palindromes
- solve:  rev+math,   difficulty 4/10
- cheese: rev+logic,  difficulty 2/10

boxes
- solve:  rev+puzzle, difficulty 4/10
- cheese: rev+crypto, difficulty 5/10
  if it is of any help, the 158B encrypted data contain (ROT47):
  E96 bgq 7=28 7@==@H65 3J 4@>A=6>6?E2CJ E6IE @? D6G6C2= =:?6D
