#!/usr/bin/env python3

import os
from hashlib import md5, blake2b

FLAG = 'FLAG{not_so_secret_flag}'


# ok, after all I liked the 1st version!..
def xor(a, b):
    assert len(a)==len(b)
    return bytes(ai^bi for ai,bi in zip(a,b))

def myhmac(a, b, h=md5):
    bs = h().block_size
    assert len(a)<=bs
    apad = a + b'\x00'*(bs-len(a))
    ipad = b'\x36'*bs
    opad = b'\x5c'*bs
    return h(xor(apad, ipad) + h(xor(apad, ipad) + b).digest()).digest()

def mysha(msg, custom_key=b'my_favorite_public_key'):
    assert len(msg)<=64
    return myhmac(msg, custom_key)


def _main():
    while True:
        key = os.urandom(16)
        print('custom_key =', key.hex())
        data1 = bytes.fromhex(input('hex1> '))
        data2 = bytes.fromhex(input('hex2> '))

        if len(data1)>64 or len(data2)>64:
            # take that!
            data1 = blake2b(data1).digest()
            data2 = blake2b(data2).digest()

        if data1!=data2 and mysha(data1, key)==mysha(data2, key):
            print(FLAG)
            break
        else:
            print('NO\n')

if __name__=='__main__':
    _main()
