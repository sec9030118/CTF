## MD5-based recipes for a Secure Hash Algorithm?

Three **cryptography** challenges around HMAC-MD5. Generate a collision to print the flag (without modifying the code, or running with `-O` to bypass `assert`).

Level 1 is a **very easy** introduction challenge.

Subsequent levels 2 and 3 are two independent **medium** challenges.

### Example server setup
Using [`ncat`](https://nmap.org/book/ncat-man.html) on port `4000`:
```
$ ncat -lkve '/usr/bin/python3 ./mysha1.py' 4000
```
