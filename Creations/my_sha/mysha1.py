#!/usr/bin/env python3

import os
import hmac

FLAG = 'FLAG{not_so_secret_flag}'


# MD5 is broken? https://github.com/corkami/collisions
# I don't like using the same hash function as everyone anyways...
# I made my own SHA using HMAC-MD5 + a public customization key.
# It's simpler and perfectly safe:
# https://crypto.stackexchange.com/questions/25584/is-hmac-md5-still-secure-for-commitment-or-other-common-uses
# https://crypto.stackexchange.com/questions/9336/is-hmac-md5-considered-secure-for-authenticating-encrypted-data
# HMAC: https://www.rfc-editor.org/rfc/rfc2104
# (MD5: https://www.rfc-editor.org/rfc/rfc1321)
def mysha(msg, custom_key=b'my_favorite_public_key'):
    return hmac.digest(msg, custom_key, 'md5')


def _main():
    while True:
        key = os.urandom(16)
        print('custom_key =', key.hex())
        data1 = bytes.fromhex(input('hex1> '))
        data2 = bytes.fromhex(input('hex2> '))
        if data1!=data2 and mysha(data1, key)==mysha(data2, key):
            print(FLAG)
            break
        else:
            print('NO\n')

if __name__=='__main__':
    _main()
