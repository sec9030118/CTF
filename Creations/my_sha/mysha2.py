#!/usr/bin/env python3

import os
import hmac

FLAG = 'FLAG{not_so_secret_flag}'


# ok, fixed!
def mysha(msg, custom_key=b'my_favorite_public_key'):
    return hmac.digest(custom_key, msg, 'md5')


def _main():
    while True:
        key = os.urandom(16)
        print('custom_key =', key.hex())
        data1 = bytes.fromhex(input('hex1> '))
        data2 = bytes.fromhex(input('hex2> '))
        if data1!=data2 and mysha(data1, key)==mysha(data2, key):
            print(FLAG)
            break
        else:
            print('NO\n')

if __name__=='__main__':
    _main()
