#!/usr/bin/env python3

from base64 import urlsafe_b64encode, urlsafe_b64decode
import json
from secrets import randbits
from hashlib import sha256
from ecdsa import SigningKey, NIST256p
from Crypto.Cipher import AES


FLAG = b'FLAG{this_is_NOT_the_real_flag!}'
assert len(FLAG)%16==0


# https://github.com/tlsfuzzer/python-ecdsa/
MY_KEY = SigningKey.generate(curve=NIST256p, hashfunc=sha256)
#print(MY_KEY.to_pem().decode())                     # private key
print(MY_KEY.get_verifying_key().to_pem().decode())  # public key


# JWT: https://www.rfc-editor.org/rfc/rfc7519
# JWS: https://www.rfc-editor.org/rfc/rfc7515
# JWA: https://www.rfc-editor.org/rfc/rfc7518
class JWT2:
    def __init__(self, payload, key=MY_KEY):
        self.header = {'alg': 'ES256', 'typ': 'JWT'}
        self.payload = payload
        self.key = key
        self.token = None

    @staticmethod
    def _enc(data):
        return urlsafe_b64encode(data).rstrip(b'=')
    @staticmethod
    def _dec(b64):
        return urlsafe_b64decode(b64+b'===')
    @staticmethod
    def _jenc(obj):
        return JWT2._enc(json.dumps(obj, separators=(',',':')).encode())
    @staticmethod
    def _jdec(b64):
        return json.loads(JWT2._dec(b64))

    def build_token(self):
        self.token = self._jenc(self.header) + b'.' + self._jenc(self.payload)
        k = randbits(256)
        sig = self.key.sign(self.token, k=k)
        self.token += b'.' + self._enc(sig)
        sig2 = self.key.sign(self.token, k=k)
        self.token += b'.' + self._enc(sig2)

    def __str__(self):
        self.build_token()
        return self.token.decode()


if __name__ == '__main__':
    secret = MY_KEY.privkey.secret_multiplier
    key = secret.to_bytes(32, 'big')
    encrypted_flag = AES.new(key, AES.MODE_ECB).encrypt(FLAG)
    token = JWT2({'flag': encrypted_flag.hex()})
    print('Token:', token)
