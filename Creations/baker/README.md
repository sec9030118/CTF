# Baker's puzzling recipe

Two **reverse** challenges about a mysterious [image](https://netpbm.sourceforge.net/doc/pgm.html) transformation.

### Level 1 - Easy rev+algo
You were told `glaf1.pgm` was generated by the following command:
```
$ ./baker 5581748991673347247 < flag1.pgm > glaf1.pgm
```
This is weird though as this should have taken an insanely long time...

Anyway, can you retrieve `flag1.pgm`?

### Level 2 - Medium rev+math
`glaf2.pgm` was generated the same way, however this time you do not know the exact command... You were only told `flag2.pgm` follows the same general format as `flag1.pgm` (only the flag text and the random noise are different).

Can you still read the flag?
