/*
  Wrapper to run the armv5-hf ELF on Raspberry Pi (1st gen. B)
  Compile on ARM arch. as PIE:
  $ gcc -pie armhf_mmap_wrapper.c
*/
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <assert.h>
//#include <errno.h>

int fd;
char buff[1<<20];

// write val @ dest
void cpy(uint dest, uint val) {
  *((uint*)dest) = val;
}

uint map_section(int offset, uint addr, uint size) {
  lseek(fd, offset, SEEK_SET);
  read(fd, buff, size);
  int fd1 = memfd_create("my.section", 0);
  write(fd1, buff, size);
  // we cannot directly use the last argument of mmap as offset
  // as it must be a multiple of the page size
  void *map = mmap((void*)addr, size, PROT_READ|PROT_WRITE|PROT_EXEC, MAP_PRIVATE, fd1, 0);
  if (addr != 0) assert((uint)map == addr);
  close(fd1);
  return (uint)map;
}

int main(int argc, char* argv[]) {
  const char *elf = "./write4_armv5-hf";
  const char *so = "./libwrite4_armv5-hf.so";
  const void (*elf_main)(void) = (void*)0x0105b8;

  // mmap ELF  (readelf -S)
  fd = open(elf, O_RDONLY);
  assert(fd > 0);
  map_section(0, 0x010000, 0xf00);
  map_section(0, 0x020000, 0x102c);
  close(fd);

  // mmap SO   (readelf -S)
  fd = open(so, O_RDONLY);
  assert(fd > 0);
  const uint lib_off = map_section(0, 0, 0xf18);
  map_section(0, lib_off+0x010000, 0x1050);
  close(fd);

  // GOT ELF   (readelf -r)
  cpy(0x02100c, lib_off+0x6e0);  // pwnme@elf-got = pwnme@lib
  cpy(0x021018, lib_off+0x7b0);  // same for print_file

  // GOT SO    (readelf -r)
  cpy(lib_off+0x011044, (uint)&stdout);
  cpy(lib_off+0x011010, (uint)&printf);
  cpy(lib_off+0x011014, (uint)&fopen);
  cpy(lib_off+0x011018, (uint)&read);
  cpy(lib_off+0x01101c, (uint)&fgets);
  cpy(lib_off+0x011020, (uint)&puts);
  cpy(lib_off+0x011028, (uint)&exit);
  cpy(lib_off+0x01102c, (uint)&setvbuf);
  cpy(lib_off+0x011030, (uint)&memset);
  cpy(lib_off+0x011034, (uint)&fclose);

  // call
  elf_main();

  return 0;
}
