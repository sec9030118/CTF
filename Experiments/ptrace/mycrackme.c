#include <stdlib.h>
#include <stdio.h>

void print_flag() {
  puts("flag{this_is_only_a_test}");
}

int main(int argc, char *argv[]) {
  if (argc > 1) {
    if (atoi(argv[1]) == 555) {
      print_flag();
    }
    else {
      puts("No");
    }
  }
  return 0;
}
