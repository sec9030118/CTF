# `ptrace()` experiments

Handmade debugging and control flow alteration.

```
$ gcc -no-pie mycrackme.c -o mycrackme
$ objdump -j .text --disassemble=main -Mintel ./mycrackme
...
0000000000401170 <main>:
  401170:	f3 0f 1e fa          	endbr64 
  401174:	55                   	push   rbp
  401175:	48 89 e5             	mov    rbp,rsp
  401178:	48 83 ec 10          	sub    rsp,0x10
  40117c:	89 7d fc             	mov    DWORD PTR [rbp-0x4],edi
  40117f:	48 89 75 f0          	mov    QWORD PTR [rbp-0x10],rsi
  401183:	83 7d fc 01          	cmp    DWORD PTR [rbp-0x4],0x1
  401187:	7e 35                	jle    4011be <main+0x4e>
  401189:	48 8b 45 f0          	mov    rax,QWORD PTR [rbp-0x10]
  40118d:	48 83 c0 08          	add    rax,0x8
  401191:	48 8b 00             	mov    rax,QWORD PTR [rax]
  401194:	48 89 c7             	mov    rdi,rax
  401197:	e8 c4 fe ff ff       	call   401060 <atoi@plt>
  40119c:	3d 2b 02 00 00       	cmp    eax,0x22b
  4011a1:	75 0c                	jne    4011af <main+0x3f>
  4011a3:	b8 00 00 00 00       	mov    eax,0x0
  4011a8:	e8 a9 ff ff ff       	call   401156 <print_flag>
  4011ad:	eb 0f                	jmp    4011be <main+0x4e>
  4011af:	48 8d 05 68 0e 00 00 	lea    rax,[rip+0xe68]        # 40201e <_IO_stdin_used+0x1e>
  4011b6:	48 89 c7             	mov    rdi,rax
  4011b9:	e8 92 fe ff ff       	call   401050 <puts@plt>
  4011be:	b8 00 00 00 00       	mov    eax,0x0
  4011c3:	c9                   	leave  
  4011c4:	c3                   	ret    
```

```
$ gcc mytracer.c -o mytracer
$ ./mytracer 5
instr: e800000000b80c75
zero flag = 0  ->  1
flag{this_is_only_a_test}
```
