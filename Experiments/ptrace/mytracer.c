#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <stddef.h>
#include <assert.h>
#include <time.h>
//#include <stdint.h>

#include <unistd.h>
#include <sys/wait.h>
#include <sys/user.h>

#include <syscall.h>
#include <sys/ptrace.h>

/*
  https://nullprogram.com/blog/2018/06/23/
  https://github.com/skeeto/ptrace-examples/blob/master/minimal_strace.c
  http://www.alexonlinux.com/how-debugger-works

  https://en.wikipedia.org/wiki/FLAGS_register
  https://en.wikipedia.org/wiki/X86_debug_register

  /usr/include/x86_64-linux-gnu/sys/ptrace.h
  /usr/include/x86_64-linux-gnu/sys/user.h
  /usr/include/x86_64-linux-gnu/sys/wait.h
  /usr/include/x86_64-linux-gnu/bits/waitstatus.h
*/


/* === Fork === */
pid_t pid;

void errxit() {
  perror("");
  exit(1);
}

void spawn_process(char *argv[]) {
  pid = fork();
  switch (pid) {
  case -1: // error
    errxit();
  case 0:  // child
    ptrace(PTRACE_TRACEME, 0, 0, 0);
    execvp("./mycrackme", argv);
    errxit();
  }
}


/* === Helpers === */
struct user_regs_struct regs;

void cont() {
  ptrace(PTRACE_CONT, pid, NULL, NULL);
  int status;
  waitpid(pid, &status, 0);
}

long set_breakpoint(const ulong addr) {
  long instr = ptrace(PTRACE_PEEKTEXT, pid, (void*)addr, NULL);
  long new_instr = (instr & ~0xff) | 0xcc;  // 0xCC -> INT 3
  ptrace(PTRACE_POKETEXT, pid, (void*)addr, new_instr);
  return instr;
}

void flip_jump(const ulong addr) {
  long instr = ptrace(PTRACE_PEEKTEXT, pid, (void*)addr, NULL);
  ptrace(PTRACE_POKETEXT, pid, (void*)addr, instr^1);
}

void unset_breakpoint(const ulong addr, long orig_instr) {
  ptrace(PTRACE_POKETEXT, pid, (void*)addr, orig_instr);
}

void get_regs() {
  ptrace(PTRACE_GETREGS, pid, NULL, &regs);
}

void set_regs() {
  ptrace(PTRACE_SETREGS, pid, NULL, &regs);
}

ulong get_rip() {
  // alternative way to read a single register, not significantly faster...
  return ptrace(PTRACE_PEEKUSER, pid, offsetof(struct user, regs.rip), NULL);
}

void step_until_addr(const ulong addr) {  // very slow
  do {
    ptrace(PTRACE_SINGLESTEP, pid, NULL, NULL);
    int status;
    waitpid(pid, &status, 0);
    assert(WIFSTOPPED(status));
    get_regs();
  } while (regs.rip != addr);
}

#define HW_DBG_REG 0  // 0-3
#define DBG_OFF_R offsetof(struct user, u_debugreg[HW_DBG_REG])
#define DBG_OFF_7 offsetof(struct user, u_debugreg[7])
void set_hardware_breakpoint(const ulong addr) {
  ptrace(PTRACE_POKEUSER, pid, DBG_OFF_R, addr);

  // check
  ulong dreg = ptrace(PTRACE_PEEKUSER, pid, DBG_OFF_R, NULL);
  //printf("dr%d = 0x%08lx\n", HW_DBG_REG, dreg);
  assert(dreg == addr);

  ulong dr7 = ptrace(PTRACE_PEEKUSER, pid, DBG_OFF_7, NULL);
  dr7 |= 1<<(2*HW_DBG_REG);                // bp local enable
  dr7 &= ~((ulong)15<<(16+4*HW_DBG_REG));  // bp condition & length
  ptrace(PTRACE_POKEUSER, pid, DBG_OFF_7, dr7);

  // check
  ulong dr7_chk = ptrace(PTRACE_PEEKUSER, pid, DBG_OFF_7, NULL);
  //printf("dr7 = 0x%08lx\n", dr7);
  assert(dr7_chk == dr7);
}

void unset_hardware_breakpoint() {
  ulong dr7 = ptrace(PTRACE_PEEKUSER, pid, DBG_OFF_7, NULL);
  dr7 &= ~((ulong)1<<(2*HW_DBG_REG));  // bp local disable
  ptrace(PTRACE_POKEUSER, pid, DBG_OFF_7, dr7);
}


/* === Attacks === */
const ulong print_flag_addr = 0x401156;
const ulong main_addr = 0x401170;
const ulong jne_addr = 0x4011a1;
const ulong atoi_got = 0x404020;


void attack1() {
  printf("[%s]\n", __func__);
  // breakpoint @jne and inverse the cmp result
  long instr = set_breakpoint(jne_addr);
  printf("instr: %lx\n", instr);
  cont();
  get_regs();
  printf("breakpoint: rip = 0x%08llx\n", regs.rip);
  printf("zero flag = %lld  ->  1\n", (regs.eflags>>6)&1);
  regs.eflags |= 1<<6;
  unset_breakpoint(jne_addr, instr);
  --regs.rip;
  set_regs();
}

void attack2() {
  printf("[%s]\n", __func__);
  // (slow) single steps until @jne
  step_until_addr(jne_addr);
  //get_regs();
  printf("zero flag = %lld  ->  1\n", (regs.eflags>>6)&1);
  regs.eflags |= 1<<6;
  set_regs();
}

void attack3() {
  printf("[%s]\n", __func__);
  // edit the jne -> je
  flip_jump(jne_addr);
}

void attack4() {
  printf("[%s]\n", __func__);
  // set rip to @print_flag
  long instr = set_breakpoint(main_addr);
  cont();
  get_regs();
  printf("breakpoint: rip = 0x%08llx\n", regs.rip);
  unset_breakpoint(main_addr, instr);
  get_regs();
  regs.rip = print_flag_addr;
  set_regs();
}

void attack5() {
  printf("[%s]\n", __func__);
  // replace atoi@got by @print_flag
  ptrace(PTRACE_POKETEXT, pid, (void*)atoi_got, print_flag_addr);
}

void attack6() {
  printf("[%s]\n", __func__);
  // hardware breakpoint @jne
  set_hardware_breakpoint(jne_addr);
  cont();
  get_regs();
  printf("breakpoint: rip = 0x%08llx\n", regs.rip);
  printf("zero flag = %lld  ->  1\n", (regs.eflags>>6)&1);
  regs.eflags |= 1<<6;
  set_regs();
  unset_hardware_breakpoint();
}


/* === MAIN === */
int main(int argc, char *argv[]) {
  srand(time(NULL));
  
  spawn_process(argv);
  waitpid(pid, 0, 0);  // sync with exec
  ptrace(PTRACE_SETOPTIONS, pid, 0, PTRACE_O_EXITKILL);

  switch ((rand()>>16)%6) {
  case 1:  attack1(); break;
  case 2:  attack2(); break;
  case 3:  attack3(); break;
  case 4:  attack4(); break;
  case 5:  attack5(); break;
  default: attack6();
  }

  cont();
  return 0;
}
