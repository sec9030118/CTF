// 64-bit
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <fcntl.h>
//#include <sys/stat.h>
#include <unistd.h>
#include <sys/mman.h>
#include <assert.h>


// toy code to force resolution of lib functions
void resolve() {
  const char *ok = "_ok";
  puts(ok+atoi("1"));
}

// write val @ dest
void cpy(uint64_t dest, uint64_t val) {
  *((uint64_t*)dest) = val;
}

int main(int argc, char *argv[]) {
  const char *exe = "./mycrackme";   // PIE
  const uint64_t addr = 0x10000000;  // arbitrary virt. @ to write the ELF
  const void (*print_flag)() = (void*)(addr+0x1169);       // @ fun to call
  const void (*fmain)(int,char**) = (void*)(addr+0x1183);  // @ main

  resolve();

  // mmap
  int fd = open(exe, O_RDONLY);
  assert(fd > 0);
  // approximate mapping (good enough here)
  void *map = mmap((void*)addr, 0x3000, PROT_READ|PROT_WRITE|PROT_EXEC, MAP_PRIVATE, fd, 0);
  assert((uint64_t)map == addr);
  map = mmap((void*)(addr+0x3000), 0x2000, PROT_READ|PROT_WRITE|PROT_EXEC, MAP_PRIVATE, fd, 0x2000);
  assert((uint64_t)map == addr+0x3000);
  close(fd);

  // GOT
  cpy(addr+0x3fc8, (uint64_t)&puts);
  cpy(addr+0x3fd0, (uint64_t)&atoi);

  // call
  if (argc > 1) fmain(argc, argv);
  else print_flag();

  return 0;
}
