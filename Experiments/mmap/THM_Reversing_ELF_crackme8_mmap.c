// compile with -m32
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/mman.h>
#include <assert.h>

// toy code to force resolution of puts & memset
void resolve() {
  char ok[] = "ok";
  memset(ok, 'O', 1);
  puts(ok);
}

// write val @ dest
void cpy(uint dest, uint val) {
  *((uint*)dest) = val;
}

int main() {
  const char *exe = "./crackme8";
  const uint addr = 0x08048000;  // virt. @ to write the ELF
  const void (*giveFlag)() = (void*)0x08048524;  // @ fun to call

  resolve();

  // mmap
  int fd = open(exe, O_RDONLY);
  assert(fd > 0);
  struct stat st;
  stat(exe, &st);
  void *map = mmap((void*)addr, st.st_size, PROT_READ|PROT_WRITE|PROT_EXEC, MAP_PRIVATE, fd, 0);
  assert((uint)map == addr);
  close(fd);

  // GOT
  cpy(0x080499cc, (uint)&puts);   // set puts @ got
  cpy(0x080499d4, (uint)&memset); // set memset @ got

  // call
  giveFlag();

  return 0;
}
