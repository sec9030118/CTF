# `mmap()` experiments

Manually map useful sections in memory, fill required relocation entries, and branch execution (call some function).

### Notes
#### General
* `MAP_FIXED` flag can be used to require an exact mapping address (properly aligned, i.e. multiple the page size).

#### About function resolution
The `resolve()` functions are used to force the lazy dynamic resolution of some libc functions (of which we need the addresses) in the GOT. However they seem useless: `gcc` already switches to non-lazy binding (relocation in `.rel.dyn` instead or `.rel.plt`, see `readelf -r`) whenever the address of a function is explicitly used in the code.

That said, there are several other ways to force the resolution:
* `export LD_BIND_NOW=1` (see `man ld.so`)
* `gcc -Wl,-z,relro,-z,now` (or simply `-z relro -z now`, see `man ld`)<br>
`readelf -d` shows the flag `BIND_NOW`<br>
the `relro` option is not strictly related to lazy-bindings, but that way `checksec` detects _Full_ RELRO


For the sake of completion, other alternatives:
* _Partial_ RELRO: `gcc -z relro -z lazy`
* _No_ RELRO: `gcc -z norelro -z lazy`



### ELF64 PIE example
Exploiting `mycrackme` compiled by:
```
$ gcc mycrackme.c -o mycrackme
```
Use `objdump -h` or `readelf -S` to see the sections mapping.
```
$ gcc mymmap.c -o mymmap
$ ./mymmap
ok
flag{this_is_only_a_test}
```

### ELF32 no PIE example
Exploiting `crackme8` from THM's _Reversing ELF_ room.
```
$ gcc -m32 THM_Reversing_ELF_crackme8_mmap.c
```

### PE32 (Windows) no PIE example on Linux
Exploiting `Level1.exe` from THM's _REloaded_ room.
```
$ gcc -m32 THM_REloaded_level1_mmap.c
```
