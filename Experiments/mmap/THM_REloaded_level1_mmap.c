// compile with -m32
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
//#include <sys/stat.h>
#include <unistd.h>
#include <sys/mman.h>
#include <assert.h>
//#include <errno.h>

int fd;
char buff[1<<20];

// toy code to force resolution of lib functions
void resolve() {
  printf("ok %x", atoi("49153"));
  puts("");
}

// write val @ dest
void cpy(uint dest, uint val) {
  *((uint*)dest) = val;
}

void map_section(int offset, uint addr, uint size) {
  lseek(fd, offset, SEEK_SET);
  read(fd, buff, size);
  int fd1 = memfd_create("my.section", 0);
  write(fd1, buff, size);
  // we cannot directly use the last argument of mmap as offset
  // as it must be a multiple of the page size
  void *map = mmap((void*)addr, size, PROT_READ|PROT_WRITE|PROT_EXEC, MAP_PRIVATE, fd1, 0);
  assert((uint)map == addr);
  close(fd1);
}

int main(int argc, char* argv[]) {
  const char *exe = "./Level1.exe";
  const void (*check)(int) = (void*)0x00401410;         // @ fun to call
  const void (*fmain)(int,char**) = (void*)0x0040143c;  // @ main

  resolve();

  // mmap (from objdump -x)
  fd = open(exe, O_RDONLY);
  assert(fd > 0);
  map_section(0x400, 0x00401000, 0xad574);    // .text
  //map_section(0xada00, 0x004af000, 0x1b24);   // .data
  map_section(0xaf600, 0x004b1000, 0xba54);   // .rdata
  //map_section(0xbb200, 0x004bd000, 0x3a688);  // .eh_fram
  map_section(0xf5a00, 0x004f9000, 0xb88);    // .idata (GOT)
  //map_section(0xf6600, 0x004fa000, 0x18);     // .CRT
  //map_section(0xf6800, 0x004fb000, 0x20);     // .tls
  close(fd);

  // GOT
  cpy(0x004f9374, (uint)&printf);
  cpy(0x004f9378, (uint)&puts);
  cpy(0x004f931c, (uint)&atoi);

  // NOP an annoying call in the main
  memset((void*)0x00401445, 0x90, 5);

  // call
  if (argc > 1) {       // call the main
    fmain(argc, argv);
  }
  else {                // brute-force the check
    for (int x=1000; x<2000; ++x) {
      printf("%d: ", x);
      check(x);
    }
  }

  return 0;
}
