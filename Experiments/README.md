# Misc.

## Deactivate ASLR
See also [here](https://gcc.gnu.org/wiki/Randomization).

#### Single process (conveniently a shell, here `bash`)
```
$ setarch $(uname -m) --addr-no-randomize /bin/bash
$ setarch -R bash
```

#### System-wide
```
$ echo 0 | sudo tee /proc/sys/kernel/randomize_va_space
$ sysctl -w kernel.randomize_va_space=0
```
Default value is `2` (full ASLR).
