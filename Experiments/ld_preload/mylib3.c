#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

void (*print_flag)() = NULL;

long _get_virtual_address(const char *perm, const uint offset, const char *name_suff) {
  int name_len = strlen(name_suff);
  long res = 0;
  char maps[128], line[512];
  sprintf(maps, "/proc/%d/maps", getpid());
  FILE *f = fopen(maps, "r");
  while (fgets(line, sizeof(line), f)!=NULL) {
    int line_len = strlen(line);
    if (line_len>=name_len && strncmp(line+line_len-name_len-1, name_suff, name_len)==0) {
      long vaddr;
      uint _offset;
      char _perm[5];
      if (sscanf(line, "%lx-%*x %4s %x", &vaddr, _perm, &_offset)==3 &&
	  strncmp(_perm, perm, 4)==0 &&
	  _offset==offset) {
	res = vaddr;
	break;
      }
    }
  }
  fclose(f);
  return res;
}

void __libc_start_main() {
  if (print_flag==NULL) {
    /*
      $ objdump -h mycrackme_pie
      $ nm mycrackme_pie
      0000000000001169 T print_flag
    */
    print_flag = (void*)(_get_virtual_address("r-xp", 0x1000, "/mycrackme_pie") + 0x169);
    printf("[print_flag @ %p]\n", &print_flag);
  }
  print_flag();
  exit(0);
}
