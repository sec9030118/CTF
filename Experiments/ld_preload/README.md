# `LD_PRELOAD` experiments

Function hijacking examples (ASLR can be assumed activated).

### No PIE versions
```
$ gcc -no-pie mycrackme.c -o mycrackme
$ nm mycrackme
...
0000000000401156 T print_flag
...
```

```
$ gcc -fPIC -shared mylib1.c -o mylib1.so
$ LD_PRELOAD=./mylib1.so ./mycrackme
flag{this_is_only_a_test}
```

#### Transparent hijack
Preserve the hijacked exported function call.
```
$ gcc -fPIC -shared mylib2.c -o mylib2.so
$ LD_PRELOAD=./mylib2.so ./mycrackme 55
>>> atoi(55)
flag{this_is_only_a_test}
<<<
No
$ LD_PRELOAD=./mylib2.so ./mycrackme 555
>>> atoi(555)
flag{this_is_only_a_test}
<<<
flag{this_is_only_a_test}
```

### PIE version
```
$ gcc -pie mycrackme.c -o mycrackme_pie
$ gcc -fPIC -shared mylib3.c -o mylib3.so
$ LD_PRELOAD=./mylib3.so ./mycrackme_pie
[print_flag @ 0x7fce7f48c088]
flag{this_is_only_a_test}
```
