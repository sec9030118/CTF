#define _GNU_SOURCE
#include <dlfcn.h>

#include <stdio.h>

const void (*print_flag)() = (void*)0x401156;

int atoi(const char *s) {
  printf(">>> atoi(%s)\n", s);
  print_flag();
  puts("<<<");

  int (*orig_atoi)(const char*);
  orig_atoi = dlsym(RTLD_NEXT, "atoi");
  return (*orig_atoi)(s);
}
