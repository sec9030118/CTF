#include <stdlib.h>

const void (*print_flag)() = (void*)0x401156;

void __libc_start_main() {
  print_flag();
  exit(0);
}
