// $ gcc -shared -nostdlib my_strcmp.c -o my_strcmp.so
#include <stdio.h>

int strcmp(const char *s1, const char *s2) {
  int i, cnt = 0;
  for (i=0; s1[i]!=0 && s2[i]!=0; ++i) {
    if (s1[i] == s2[i]) ++cnt;
    else break;
  }
  printf("<strcmp:%d>\n", cnt);
  return (int)s1[i] - (int)s2[i];
}
