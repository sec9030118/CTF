# Callgrind
```
$ valgrind --tool=callgrind my-command
```
Simple example of using Valgrind's [Callgrind](https://valgrind.org/docs/manual/cl-manual.html) to count instructions for local timing attacks.

### Compile & run
```
$ gcc vultim.c -o vultim
$ python3 attack.py
```

### Remarks
* Depending on the function used for comparison (_ad hoc_ implementation, VM-based, `strcmp`/`memcmp`, etc), it might not work, at least byte-by-byte, as several bytes might be compared at once, or the order might be reversed...
* If an imported function is used for the comparison (e.g. `strcmp`), it could be more appropriate (and faster) to hijack it (e.g. through `LD_PRELOAD`) to leak the matching prefix length and use is as measurement for the attack.
```
$ gcc -shared -nostdlib my_strcmp.c -o my_strcmp.so
$ python3 hijack_attack.py
```

### See also
* Intel [Pin](https://www.intel.com/content/www/us/en/developer/articles/tool/pin-a-dynamic-binary-instrumentation-tool.html)
