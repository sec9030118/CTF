#!/usr/bin/env python3

import sys
import subprocess
import string
import re

ALPHA = string.ascii_lowercase
SIZE = 5

PWD  = [ALPHA[0]]*SIZE
for i in range(SIZE):
    for a in ALPHA:
        PWD[i] = a
        pwd = ''.join(PWD)
        print('>', pwd, end='\r', flush=True)
        proc = subprocess.run(
            f'LD_PRELOAD=./my_strcmp.so ./vultim {pwd}',
            shell=True, text=True, stdout=subprocess.PIPE
        )
        if 'YES' in proc.stdout:
            print()
            sys.exit()
        cnt = int(re.search(r'<strcmp:(\d+)>', proc.stdout, re.M).group(1))
        if cnt>i:
            break
