#include <stdio.h>
#include <string.h>

int main(int argc, char *argv[]) {
  char PWD[] = "mypwd";  // to avoid string litterals
  if (strcmp(PWD, argv[1]) == 0) puts("YES");
  else puts("NO");
  return 0;
}
