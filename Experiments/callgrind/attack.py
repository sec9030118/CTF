#!/usr/bin/env python3

import sys
import subprocess
import string
import re

alpha = string.ascii_lowercase

pwd = ''
while True:
    max_cnt = -1
    max_a = ''
    for a in alpha:
        print(f'> {pwd}{a}', end='\r', flush=True)
        proc = subprocess.run(
            ('valgrind', '--tool=callgrind', '--callgrind-out-file=/dev/null', './vultim', pwd+a),
            text=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE
        )
        if 'YES' in proc.stdout:
            print()
            sys.exit()
        cnt = int(re.search(r'Collected : (\d+)', proc.stderr, re.M).group(1))
        if cnt > max_cnt:
            max_cnt = cnt
            max_a = a
    pwd += max_a
