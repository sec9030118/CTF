#!/bin/sh

BROWSER=/usr/bin/firefox
URL='http://factordb.com/index.php?query='

if [ $# -eq 0 ]; then
    echo "usage: $0 n [n...]" >&2
    exit 1
fi

for n in "$@"; do
    $BROWSER "$URL$n"
done
