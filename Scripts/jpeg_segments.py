#!/usr/bin/env python3

import sys
import shutil

_width, _ = shutil.get_terminal_size()


# References:
#  https://github.com/corkami/pics/blob/master/binary/JPG.png


def jpeg_segments(data):
    print('-- JPEG '.ljust(_width, '-'))
    MARKERS = {
        b'\xFF\xD8': 'SOI',  # Start of Image
        b'\xFF\xDA': 'SOS',  # Start of Scan
        b'\xFF\xD9': 'EOI',  # End of Image
        b'\xFF\xFE': 'COM',  # Comment
        b'\xFF\xE0': 'APP0',
        b'\xFF\xE1': 'APP1',
        b'\xFF\xDB': 'DQT',
        b'\xFF\xC0': 'SOF0',
        b'\xFF\xC2': 'SOF2',
        b'\xFF\xC4': 'DHT'
    }
    i = 0
    while i<len(data):
        marker = data[i:i+2]
        i += 2
        assert marker[0]==0xFF, 'invalid segment marker'
        seg = MARKERS.get(marker, '???')
        if seg=='SOI':
            print(f'{i:06x}: {marker.hex()} {seg:4}')
            continue
        if seg=='EOI':
            print(f'{i:06x}: {marker.hex()} {seg:4}')
            break
        size = int.from_bytes(data[i:i+2], 'big')
        print(f'{i:06x}: {marker.hex()} {seg:4} {size}B')
        assert size>=2, 'invalid segment size'
        i += size
        if seg=='SOS':
            print('... jumping to EOI ...')
            i = data.index(b'\xFF\xD9', i)
    print(f'-- file ended at {i}/{len(data)}B '.ljust(_width, '-'))


def _main():
    for fname in sys.argv[1:]:
        print('>', fname)
        with open(fname, 'rb') as f:
            img = f.read()
        jpeg_segments(img)
        print()

if __name__=='__main__':
    _main()
