#!/usr/bin/env python3

import sys
import argparse
import shutil
from sage.all import Integer, PolynomialRing, ZZ
_W,_ = shutil.get_terminal_size()


WEAK_THRESH = 0.1  # at least 10% of 0s


def check_factors(F, B):
    cnt = sum(m for _,m in F)
    if cnt>1:
        print()
        print(f'Found {cnt} factors:')
        for p,m in F:
            f = p(B)
            fstr = f'{f}^{m}' if m>1 else str(f)
            if not f.is_prime(proof=False):
                fstr += ' (composite)'
            print(fstr)
        sys.exit()


def _main():
    parser = argparse.ArgumentParser(description='Polynomial-based integer factorization (for CTF weak RSA moduli)')
    parser.add_argument('n', help='integer')
    parser.add_argument('--base-start', type=int, default=2)
    parser.add_argument('--powers', '-p', action='store_true', help='use powers of the base')
    parser.add_argument('--skip-weak', action='store_true', help=f'skip bases with < {100.*WEAK_THRESH}%% of 0s')
    parser.add_argument('--balanced', action='store_true', help='use balanced decomposition')
    args = parser.parse_args()

    N = Integer(args.n)
    print('Factoring n =', N)
    if N.is_prime(proof=False):
        print('n is prime')
        sys.exit()

    P = PolynomialRing(ZZ, 'x')

    B = args.base_start-1
    assert B>=1
    while True:
        B += 1

        if args.balanced and B>2: D = N.balanced_digits(B)
        else:                     D = N.digits(B)
        if len(D)<2:
            break
        z = D.count(0)/len(D)
        if args.skip_weak and z<WEAK_THRESH:
            continue

        print(f'> base {B} ({100.*z:.2f}% of 0s)'.ljust(_W), end='\r', flush=True)

        pol = P(D)
        F = pol.factor()
        check_factors(F, B)

        if args.powers:
            K = 1
            BK = B
            while True:
                K += 1
                BK *= B
                if args.balanced: D = N.balanced_digits(BK)
                else:             D = N.digits(BK)
                if len(D)<2:
                    break
                print(f'> base {B}^{K}'.ljust(_W), end='\r', flush=True)
                pol = P(D)
                F = pol.factor()
                check_factors(F, BK)


if __name__=='__main__':
    try:
        _main()
    except KeyboardInterrupt:
        print()
