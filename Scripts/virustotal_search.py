#!/usr/bin/env python3

import sys
from hashlib import sha256
import webbrowser

URL = 'https://www.virustotal.com/gui/search/'

def fhash(fname, bsize=1<<20):
    h = sha256()
    with open(fname, 'rb') as f:
        fdata = f.read(bsize)
        while len(fdata)>0:
            h.update(fdata)
            fdata = f.read(bsize)
    return h.hexdigest()

if __name__=='__main__':
    if len(sys.argv)==1:
        print(f'usage: {sys.argv[0]} file [file...]', file=sys.stderr)
        sys.exit(1)
    for fname in sys.argv[1:]:
        webbrowser.open(URL + fhash(fname))
