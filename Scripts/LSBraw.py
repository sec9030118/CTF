#!/usr/bin/env python3

import sys
import argparse
from os.path import basename, splitext
from PIL import Image
from itertools import combinations, permutations


PREVIEW_LEN = 32
OUTBASE = '/tmp/lsb'


def raw_L(img, mirror=False, outbase=OUTBASE):
    assert img.mode == 'L'
    bits = ''.join(str(col&1) for col in img.getdata())
    if mirror:
        data1 = bytes(int(bits[i:i+8][::-1], 2) for i in range(0, len(bits), 8))
    else:
        data1 = bytes(int(bits[i:i+8], 2) for i in range(0, len(bits), 8))
    outname = f'{outbase}_1.bin'
    with open(outname, 'wb') as f:
        f.write(data1)
    print(f'written {outname} <{data1[:PREVIEW_LEN]}...>')

def raw_RGB(img, mirror=False, outbase=OUTBASE):
    assert img.mode.startswith('RGB')
    data = img.getdata()
    bits = ''.join(f'{col[0]&1}{col[1]&1}{col[2]&1}' for col in data)
    if mirror:
        data1 = bytes(int(bits[i:i+8][::-1], 2) for i in range(0, len(bits), 8))
    else:
        data1 = bytes(int(bits[i:i+8], 2) for i in range(0, len(bits), 8))
    outname = f'{outbase}_RGB.bin'
    with open(outname, 'wb') as f:
        f.write(data1)
    print(f'written {outname} <{data1[:PREVIEW_LEN]}...>')


def raw_split_RGB(img, mirror=False, outbase=OUTBASE):
    assert img.mode.startswith('RGB')
    data = img.getdata()
    for i,chan in enumerate(img.mode):
        bits = ''.join(str(col[i]&1) for col in data for c in col)
        if mirror:
            data1 = bytes(int(bits[i:i+8][::-1], 2) for i in range(0, len(bits), 8))
        else:
            data1 = bytes(int(bits[i:i+8], 2) for i in range(0, len(bits), 8))
        outname = f'{outbase}_{chan}.bin'
        with open(outname, 'wb') as f:
            f.write(data1)
        print(f'written {outname} <{data1[:PREVIEW_LEN]}...>')

def raw_brute_RGB(img, outbase=OUTBASE):
    assert img.mode.startswith('RGB')
    nchan = len(img.mode)
    data = img.getdata()
    for r in range(1, nchan+1):
        if r==2: continue  # skipping very unlikely combinations
        for C in combinations(range(nchan), r):
            for P in permutations(C):
                bits = ''.join(''.join(str(col[i]&1) for i in P) for col in data)
                data1_ = bytes(int(bits[i:i+8], 2) for i in range(0, len(bits), 8))
                data1m = bytes(int(bits[i:i+8][::-1], 2) for i in range(0, len(bits), 8))
                for bord,data1 in (('_',data1_), ('m',data1m)):
                    outname = f'{outbase}_{"".join(img.mode[i] for i in P)}{bord}.bin'
                    with open(outname, 'wb') as f:
                        f.write(data1)
                    print(f'written {outname} <{data1[:PREVIEW_LEN]}...>')


def _main():
    parser = argparse.ArgumentParser(description='LSB Steganography - Raw binary output')
    parser.add_argument('imgname', nargs='+')
    parser.add_argument('--split', '-s', action='store_true', help='split RGB mode')
    parser.add_argument('--mirror', '-m', action='store_true', help='reverse 8-bit ordering')
    parser.add_argument('--brute', '-b', action='store_true', help='try all combinations/permutations')
    parser.add_argument('--output-base', '-o', default=OUTBASE)
    args = parser.parse_args()

    for imgname in args.imgname:
        print('>', imgname)
        img = Image.open(imgname)
        print('mode', img.mode, img.size)
        if img.mode == 'P':
            print('/!\\ Palette mode: Converting to RGB but LSB steg. unlikely', file=sys.stderr)
            img = img.convert('RGB')
        outbase = f'{args.output_base}_{splitext(basename(imgname))[0]}'
        if args.brute:
            raw_brute_RGB(img, outbase=outbase)
        elif args.split:
            raw_split_RGB(img, mirror=args.mirror, outbase=outbase)
        else:
            if img.mode == 'L':
                raw_L(img, mirror=args.mirror, outbase=outbase)
            else:
                raw_RGB(img, mirror=args.mirror, outbase=outbase)

if __name__=='__main__':
    _main()
