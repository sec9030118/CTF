#!/usr/bin/env python3

import sys
import argparse
import numpy as np

SEP = '\n[[XOROMATIC_{}_XOROMATIC]]\n'


def self_xor(DATA, mirror=False, quiet=False):
    B = DATA[::-1] if mirror else DATA
    for i in range(1, len(DATA)):
        if not quiet:
            delim = SEP.format(f'SELF_{i}')
            sys.stdout.buffer.write(delim.encode())
        X = DATA[:-i]^B[i:]
        sys.stdout.buffer.write(bytes(X))

def cycle_xor(DATA, keysize, KEYSRC=None, keystep=1, mirror=False, quiet=False):
    if KEYSRC is None:
        KEYSRC = DATA
    B = DATA[::-1] if mirror else DATA
    q = (len(DATA)+keysize-1)//keysize
    for i in range(0, len(KEYSRC)-keysize+1, keystep):
        K = np.tile(KEYSRC[i:i+keysize], q)[:len(DATA)]
        for d in range(keysize):
            if not quiet:
                delim = SEP.format(f'CYCLE_{i}_{d}')
                sys.stdout.buffer.write(delim.encode())
            X = B[d:]^K[:len(K)-d]
            sys.stdout.buffer.write(bytes(X))


def _main():
    parser = argparse.ArgumentParser(description='XOR Brute/Cheese tool')
    parser.add_argument('filename')
    parser.add_argument('--quiet', '-q', action='store_true')
    parser.add_argument('--cycle', '-c', type=int, help='cycle-mode key size')
    parser.add_argument('--key-step', '-ks', type=int, default=1)
    parser.add_argument('--key-file', '-kf', help='optional alternative key file')
    parser.add_argument('--mirror', '-r', action='store_true')
    args = parser.parse_args()

    with open(args.filename, 'rb') as f:
        DATA = np.array(list(f.read()), dtype=np.uint8)

    if args.cycle is not None:
        KEYSRC = None
        if args.key_file is not None:
            with open(args.key_file, 'rb') as f:
                KEYSRC = np.array(list(f.read()), dtype=np.uint8)
        cycle_xor(DATA, args.cycle, KEYSRC=KEYSRC, keystep=args.key_step, quiet=args.quiet)
    else:
        self_xor(DATA, quiet=args.quiet)

if __name__=='__main__':
    _main()
