#!/usr/bin/env python3

import sys
import argparse
import random
import pygame


FPS = 15
OUTNAME = '/tmp/current.png'
COL_SELECT= (255,80,80)


CONTROLS = '''
Controls:
  Mouse L      select/swap pieces, or rows/columns if [Shift]
  Mouse R      unselect
  [p]          print permutation
  [s]          save image
  [r]          randomized shuffle
  Arrows       rotate image, or selected row/column, on a torus
  [Backspace]  cancel move
  [F1]/[h]     help
  [Esc]        quit
'''
def print_controls():
    print(CONTROLS)


def main():
    parser = argparse.ArgumentParser(description='Play (& create) image mosaic puzzles')
    parser.add_argument('rows', type=int)
    parser.add_argument('columns', type=int)
    parser.add_argument('imgfile')
    args = parser.parse_args()

    pygame.init()

    R,C = args.rows, args.columns
    orig = pygame.image.load(args.imgfile)
    FW,FH = orig.get_size()
    BW,BH = FW//C, FH//R
    if not FW%C==FH%R==0:
        print(f'[!] Image size {FW}x{FH} not multiple of {C}x{R}', file=sys.stderr)

    def p2i(i,j):
        return (i%R)*C + (j%C)
    def i2p(k):
        return divmod(k, C)

    screen = pygame.display.set_mode((FW,FH))
    pygame.display.set_caption('Puzzle')

    orig = orig.convert()
    curr = orig.copy()
    rects = [pygame.Rect(j*BW,i*BH, BW,BH) for i in range(R) for j in range(C)]
    blocks = [orig.subsurface(r) for r in rects]  # pieces surfaces
    perm = list(range(R*C))  # current permutation
    selected = None
    history = []

    def print_permutation():
        print('Permutation = [')
        print(',\n'.join(','.join(f'{x:3d}' for x in perm[i:i+C]) for i in range(0, R*C, R)))
        print(']')

    print_controls()

    CLK = pygame.time.Clock()
    while True:
        for event in pygame.event.get():
            if event.type==pygame.QUIT:
                return
            elif event.type==pygame.KEYDOWN:
                if event.key==pygame.K_ESCAPE:
                    return
                elif event.key==pygame.K_h or event.key==pygame.K_F1:  # help
                    print_controls()
                elif event.key==pygame.K_p:
                    print_permutation()
                elif event.key==pygame.K_r:  # shuffle
                    history.append(tuple(perm))
                    random.shuffle(perm)
                elif event.key==pygame.K_s:  # save
                    print_permutation()
                    pygame.image.save(curr, OUTNAME)
                    print('Saved to', OUTNAME)
                elif event.key==pygame.K_BACKSPACE:  # back
                    if history:
                        perm = list(history.pop())
                    else:
                        print('[!] empty history', file=sys.stderr)
                elif event.key==pygame.K_UP:    # rotate U
                    history.append(tuple(perm))
                    if selected is None:
                        perm = [perm[p2i(i+1,j)] for i in range(R) for j in range(C)]
                    else:  # single column
                        _,sj = i2p(selected)
                        perm = [perm[p2i((i+1 if j==sj else i),j)] for i in range(R) for j in range(C)]
                elif event.key==pygame.K_DOWN:  # rotate D
                    history.append(tuple(perm))
                    if selected is None:
                        perm = [perm[p2i(i-1,j)] for i in range(R) for j in range(C)]
                    else:  # single column
                        _,sj = i2p(selected)
                        perm = [perm[p2i((i-1 if j==sj else i),j)] for i in range(R) for j in range(C)]
                elif event.key==pygame.K_LEFT:  # rotate L
                    history.append(tuple(perm))
                    if selected is None:
                        perm = [perm[p2i(i,j+1)] for i in range(R) for j in range(C)]
                    else:  # single row
                        si,_ = i2p(selected)
                        perm = [perm[p2i(i,(j+1 if i==si else j))] for i in range(R) for j in range(C)]
                elif event.key==pygame.K_RIGHT:  # rotate R
                    history.append(tuple(perm))
                    if selected is None:
                        perm = [perm[p2i(i,j-1)] for i in range(R) for j in range(C)]
                    else:  # single row
                        si,_ = i2p(selected)
                        perm = [perm[p2i(i,(j-1 if i==si else j))] for i in range(R) for j in range(C)]
            elif event.type==pygame.MOUSEBUTTONDOWN:
                if event.button==1:
                    x,y = event.pos
                    ei,ej = y//BH, x//BW
                    if 0<=ei<R and 0<=ej<C:
                        ek = ei*C+ej
                        if selected is None:
                            selected = ek
                            print(f'<selected {ek}>')
                        elif selected==ek:
                            selected = None
                        else:
                            mod = pygame.key.get_mods() & pygame.KMOD_SHIFT
                            si,sj = i2p(selected)
                            history.append(tuple(perm))
                            if mod and ei==si:    # swap columns
                                for i in range(R):
                                    k1,k2 = p2i(i,ej), p2i(i,sj)
                                    perm[k1], perm[k2] = perm[k2], perm[k1]
                            elif mod and ej==sj:  # swap lines
                                for j in range(C):
                                    k1,k2 = p2i(ei,j), p2i(si,j)
                                    perm[k1], perm[k2] = perm[k2], perm[k1]
                            else:  # swap pieces
                                perm[ek], perm[selected] = perm[selected], perm[ek]
                            selected = None
                elif event.button==3:
                    selected = None

        # update current state
        for k in range(R*C):
            curr.blit(blocks[perm[k]], rects[k])
        screen.blit(curr, (0, 0))

        # draw selection
        if selected is not None:
            i,j = divmod(selected, C)
            pygame.draw.circle(screen, COL_SELECT, (j*BW+BW//2,i*BH+BH//2), 3*min(BH,BW)//8, min(BH,BW)//8)

        pygame.display.flip()
        CLK.tick(FPS)


if __name__=='__main__':
    main()
