#!/usr/bin/env python3

import sys
import shutil
import argparse
import zlib

_width, _ = shutil.get_terminal_size()


# References:
#  http://www.libpng.org/pub/png/spec/1.2/PNG-Structure.html
#  https://www.w3.org/TR/2003/REC-PNG-20031110/
#  http://www.libpng.org/pub/png/book/
#  https://github.com/corkami/pics/blob/master/binary/PNG.png


def png_chunks(data, ignore_errors=False):
    HDR = b'\x89PNG\r\n\x1a\n' + b'\x00\x00\x00\x0dIHDR'
    hdr = data[:len(HDR)]
    if hdr!=HDR:
        print(f'/!\\ incorrect PNG header: found {hdr} / expected {HDR}', file=sys.stderr)
        if not ignore_errors: sys.exit(1)
    print('-- PNG '.ljust(_width, '-'))
    i = 8
    while i<len(data):
        i0 = i
        clen = int.from_bytes(data[i:i+4], 'big')
        i += 4
        ctyp = data[i:i+4]
        i += 4
        print(f'{i0:06x}: {ctyp} {clen}B')
        cdat = data[i:i+clen]
        if ctyp==b'tEXt':
            key,val = cdat.split(b'\x00')
            print(f'        {key} : {val}')
        i += clen
        crc = int.from_bytes(data[i:i+4], 'big')
        cexp = zlib.crc32(ctyp+cdat)
        if crc!=cexp:
            print(f'/!\\ incorrect checksum: found {crc:08x} / expected {cexp:08x}', file=sys.stderr)
            if not ignore_errors: sys.exit(1)
        i += 4
        if ctyp==b'IEND':
            break
    print(f'-- file ended at {i}/{len(data)}B '.ljust(_width, '-'))


def _main():
    parser = argparse.ArgumentParser(description='Dissect PNG files (basis for CTF scripts).')
    parser.add_argument('pngfile', nargs='+')
    parser.add_argument('--ignore-errors', '-I', action='store_true', help='do not exit on errors')
    args = parser.parse_args()

    for fname in args.pngfile:
        print('>', fname)
        with open(fname, 'rb') as f:
            img = f.read()
        png_chunks(img, args.ignore_errors)
        print()

if __name__=='__main__':
    _main()
