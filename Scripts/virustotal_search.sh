#!/bin/sh

BROWSER=/usr/bin/firefox
URL='https://www.virustotal.com/gui/search/'

if [ $# -eq 0 ]; then
    echo "usage: $0 file [file...]" >&2
    exit 1
fi

for f in "$@"; do
    h=$(sha256sum "$f" | cut -d' ' -f1)
    $BROWSER "$URL$h"
done
