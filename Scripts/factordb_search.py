#!/usr/bin/env python3

import sys
import webbrowser

URL = 'http://factordb.com/index.php?query='

if __name__=='__main__':
    if len(sys.argv)==1:
        print(f'usage: {sys.argv[0]} n [n...]', file=sys.stderr)
        sys.exit(1)
    for n in sys.argv[1:]:
        if n.startswith('0x'):
            n = str(int(n, 16))
        webbrowser.open(URL + n)
