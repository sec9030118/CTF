#!/usr/bin/env python3

import sys
import re
import argparse


def get_mapping(pid, verbose=True):
    map_file = f'/proc/{pid}/maps'
    mapping = []
    with open(map_file, 'r') as fmap:
        for i,line in enumerate(fmap):
            mo = re.match(r'([0-9a-fA-F]+)-([0-9a-fA-F]+)\s+[-r][-w][-x]', line)
            start = int(mo.group(1), 16)
            end   = int(mo.group(2), 16)
            mapping.append((start, end-start, line))
            if verbose:
                print(f'{i:3d}: {line.rstrip()}')
    return mapping


def dump_areas(pid, mapping, areas, verbose=True):
    mem_file = f'/proc/{pid}/mem'
    out_file = f'{pid}.dump'
    with open(mem_file, 'rb', 0) as fmem, open(out_file, 'wb') as fout:
        if verbose:
            print('Dumping areas:')
        for i in areas:
            try:
                start, size, line = mapping[i]
            except IndexError:
                print(f'[SKIPPED] area #{i} does not exist', file=sys.stderr)
                continue
            if verbose:
                print(f'{i:3d}: {line.rstrip()}')
            fmem.seek(start)
            try:
                fout.write(fmem.read(size))
            except OSError:
                print(f'[ERROR] area #{i} (from {start:x}) skipped', file=sys.stderr)
                continue
    if verbose:
        print(f'Dump saved to: {out_file}')


def _main():
    parser = argparse.ArgumentParser(description='Dump virtual memory areas of a process to a file')
    parser.add_argument('pid')
    parser.add_argument('--dump', '-D', help='indices of areas to dump (comma separated, ranges allowed, "all" for all)')
    args = parser.parse_args()

    if args.dump is not None:
        if args.dump!='all':
            areas = []
            for a in args.dump.split(','):
                if '-' in a:
                    l,r = map(int, a.split('-'))
                    areas += range(l, r+1)
                else:
                    areas.append(int(a))
        mapping = get_mapping(args.pid, verbose=False)
        if args.dump=='all':
            areas = tuple(range(len(mapping)))
        dump_areas(args.pid, mapping, areas)

    else:
        get_mapping(args.pid)

if __name__=='__main__':
    _main()
