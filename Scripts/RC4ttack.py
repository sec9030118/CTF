#!/usr/bin/env python3

import sys
import argparse
import numpy as np
from itertools import islice
from collections import defaultdict

SEP = '\n[[RC4TTACK_{}_RC4TTACK]]\n'


def rc4_init(key):
    S = list(range(256))
    j = 0
    for i in range(256):
        j = (j + S[i] + key[i%len(key)]) % 256
        S[i],S[j] = S[j],S[i]
    return S

def rc4_stream(S, drop=0):
    i = j = 0
    for _ in range(drop):
        i = (i + 1) % 256
        j = (j + S[i]) % 256
        S[i],S[j] = S[j],S[i]
    while True:
        i = (i + 1) % 256
        j = (j + S[i]) % 256
        S[i],S[j] = S[j],S[i]
        yield S[(S[i] + S[j]) % 256]

def rc4_xor(S, msg):
    return bytes(a^k for a,k in zip(msg, rc4_stream(S)))

def rc4_crypt(key, msg):
    return rc4_xor(rc4_init(key), msg)

def _test():
    import random
    assert rc4_crypt(b'Secret', b'Attack at dawn') == bytes.fromhex('45A01F645FC35B383552544B9BF5')
    testfile = '/tmp/rc4test.bin'
    S = rc4_init(b'secret')
    with open(testfile, 'wb') as f:
        f.write(random.randbytes(random.randrange(1<<8)))
        f.write(bytes(S))
        f.write(random.randbytes(random.randrange(1<<8)))
        f.write(rc4_xor(S, b'flag{this_is_only_a_test}'))
        f.write(random.randbytes(random.randrange(1<<8)))
    sys.stderr.write(f'written {testfile}\n')


def find_states(data):
    count = [0]*256
    ones = 0
    for i in range(len(data)):
        if i>=256:
            b = data[i-256]
            if count[b]==1: ones -= 1
            count[b] -= 1
            if count[b]==1: ones += 1
        b = data[i]
        if count[b]==1: ones -= 1
        count[data[i]] += 1
        if count[b]==1: ones += 1
        if ones==256:
            l = i-255
            sys.stderr.write(f'possible state at {hex(l)}: {data[l:l+256]}\n')
            yield l


def key_rc4(data, keysize=16, keysrc=None, keystep=1, drop=0, mirror=False, quiet=False):
    if keysrc is None:
        keysrc = data
    DATA = np.array(list(data), dtype=np.uint8)
    B = DATA[::-1] if mirror else DATA
    for i in range(0, len(keysrc)-keysize+1, keystep):
        key = keysrc[i:i+keysize]
        S = rc4_init(key)
        K = np.array(list(islice(rc4_stream(S, drop), len(DATA))), dtype=np.uint8)
        for d in range(len(DATA)):
            if not quiet:
                delim = SEP.format(f'KEY_{i}_{d}')
                sys.stdout.buffer.write(delim.encode())
            X = B[d:]^K[:len(K)-d]
            sys.stdout.buffer.write(bytes(X))


def state_rc4(data, state_offsets, drop=0, mirror=False, quiet=False):
    DATA = np.array(list(data), dtype=np.uint8)
    B = DATA[::-1] if mirror else DATA
    for i in state_offsets:
        S = list(data[i:i+256])
        K = np.array(list(islice(rc4_stream(S, drop), len(DATA))), dtype=np.uint8)
        for d in range(len(DATA)):
            if not quiet:
                delim = SEP.format(f'STATE_{i}_{d}')
                sys.stdout.buffer.write(delim.encode())
            X = B[d:]^K[:len(K)-d]
            sys.stdout.buffer.write(bytes(X))


def brute_states(data, state_offsets, wordlist):
    DB = defaultdict(list)
    for i in state_offsets:
        DB[data[i:i+256]].append(i)
    with open(wordlist, 'rb') as f:
        for line in f:
            w = line.rstrip(b'\n')
            S = bytes(rc4_init(w))
            if S in DB:
                print(f'state({w}) at: {", ".join(map(hex, DB[S]))}')


def _main():
    #_test()
    parser = argparse.ArgumentParser(description='RC4 Brute/Cheese tool')
    parser.add_argument('filename')
    # general arguments
    parser.add_argument('--quiet', '-q', action='store_true')
    parser.add_argument('--mirror', '-r', action='store_true')
    parser.add_argument('--drop', '-d', type=int, default=0)
    # key mode arguments
    parser.add_argument('--key-size', '-ks', type=int, default=16)
    parser.add_argument('--key-file', '-kf')
    #parser.add_argument('--key-step', type=int, default=1)
    # state modes
    parser.add_argument('--state', '-S', choices=('list', 'decrypt', 'brute'))
    parser.add_argument('--wordlist', '-w', default='/usr/share/dict/words')
    args = parser.parse_args()

    with open(args.filename, 'rb') as f:
        data = f.read()

    if args.state is None:
        keysrc = None
        if args.key_file is not None:
            with open(args.key_file, 'rb') as f:
                keysrc = f.read()
        key_rc4(data, keysize=args.key_size, keysrc=keysrc, drop=args.drop, mirror=args.mirror, quiet=args.quiet)
    else:
        state_offsets = list(find_states(data))
        sys.stderr.flush()
        sys.stdout.flush()
        if args.state=='decrypt':
            state_rc4(data, state_offsets, drop=args.drop, mirror=args.mirror, quiet=args.quiet)
        elif args.state=='brute':
            brute_states(data, state_offsets, args.wordlist)

if __name__=='__main__':
    _main()
