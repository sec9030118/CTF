#!/usr/bin/env python3

import sys
import argparse
from os.path import basename, splitext
from PIL import Image


# Merge operation
# c is a color tuple of size 3/4 for RGB/RGBA
MERGE_OP = (lambda c: c[0]^c[1]^c[2])

OUTBASE = '/tmp/lsb'


def split_L(img, outbase=OUTBASE, outext='png'):
    assert img.mode == 'L'
    img1 = Image.new('1', img.size)
    data1 = [col&1 for col in img.getdata()]
    img1.putdata(data1)
    outname = f'{outbase}_1.{outext}'
    img1.save(outname)
    print('written', outname)

def split_RGB(img, outbase=OUTBASE, outext='png'):
    assert img.mode.startswith('RGB')
    data = img.getdata()
    for i,chan in enumerate(img.mode):
        img1 = Image.new('1', img.size)
        data1 = [col[i]&1 for col in data]
        img1.putdata(data1)
        outname = f'{outbase}_{chan}.{outext}'
        img1.save(outname)
        print('written', outname)


def merge_RGB(img, op=MERGE_OP, outbase=OUTBASE, outext='png'):
    assert img.mode.startswith('RGB')
    img1 = Image.new('1', img.size)
    data1 = [op(col) for col in img.getdata()]
    img1.putdata(data1)
    outname = f'{outbase}_merge.{outext}'
    img1.save(outname)
    print('written', outname)


def _main():
    parser = argparse.ArgumentParser(description='LSB Steganography - Image output')
    parser.add_argument('imgname', nargs='+')
    parser.add_argument('--merge', '-m', action='store_true')
    parser.add_argument('--output-base', '-o', default=OUTBASE)
    args = parser.parse_args()

    for imgname in args.imgname:
        print('>', imgname)
        img = Image.open(imgname)
        print('mode', img.mode, img.size)
        if img.mode == 'P':
            print('/!\\ Palette mode: Converting to RGB but LSB steg. unlikely', file=sys.stderr)
            img = img.convert('RGB')
        outbase = f'{args.output_base}_{splitext(basename(imgname))[0]}'
        if args.merge:
            merge_RGB(img, outbase=outbase)
        else:
            if img.mode == 'L':
                split_L(img, outbase=outbase)
            else:
                split_RGB(img, outbase=outbase)

if __name__=='__main__':
    _main()
