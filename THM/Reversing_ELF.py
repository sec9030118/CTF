## Crackme1-3 are trivial

## Crackme4
pwd  = 0x7b175614497b5d49.to_bytes(8, 'little')
pwd += 0x547b175651474157.to_bytes(8, 'little')
pwd += 0x4053.to_bytes(2, 'little')
pwd = bytes(b^0x24 for b in pwd)
print(pwd)

## Crackme5
print(b'\x4f\x66\x64\x6c\x44\x53\x41\x7c\x33\x74\x58\x62\x33\x32\x7e\x58\x33\x74\x58\x40\x73\x58\x60\x34\x74\x58\x74\x7a')

## Crackme6
# my_secure_test() compares the input char-by-char to "1337_pwd"

## Crackme7
# entering (0x7a69 =) 31337 as menu choice calls giveFlag()

## Crackme8
# giving (-0x35010ff3 =) "-889262067" as parameter calls giveFlag()
