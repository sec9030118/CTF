#!/usr/bin/env python3

import sys
_print = sys.stdout.buffer.write
from telnetlib import Telnet

# 0123456789abcdef0123456789abcdef0123456789abcdef
# access_username=admio&password=sUp3rPaSs1
#            bit flip ^

tn = Telnet('10.10.164.241', 1337)

_print(tn.read_until(b': '))
tn.write(b'admio\n')
print('admio')

_print(tn.read_until(b': '))
tn.write(b'sUp3rPaSs1\n')
print('sUp3rPaSs1')

_print(tn.read_until(b': '))
ciph = tn.read_until(b'\n').decode().strip()
print(ciph)
ciph = bytearray.fromhex(ciph)
ciph[4] ^= 1

_print(tn.read_until(b': '))
print(ciph.hex())
tn.write(ciph.hex().encode()+b'\n')

_print(tn.read_all())
print()
