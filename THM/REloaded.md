# REloaded

These are PE 32-bit. We mostly used static analysis with Ghidra.

### Level 0
The flag appears in clear in the `.rdata` section and is referenced in the code of the code of `FUN_00401410`, which checks the flag and prints the output.

### Level 1
Starting from `.rdata` XREFs, we find `FUN_00401410` which checks the flag by comparing the first parameter given to the program to `0x6ad = 1709`.

### Level 2
Starting again from `.rdata` XREFs, we find `FUN_00401410` (again!) which compares the first parameter to the flag `L3_1s_20t_Th3_L131t` written as local variables in the stack.

But we were actually expected to bypass that check by inversing the jump at `0x0040144d`: `JNZ / 75 21 -> JZ / 74 21`.

### Level 3
We locate `FUN_004014af`, which contains the flag `THMctf-L4` (to validate the challenge) as local variable in the stack. Note however that that flag is then xored with `0x07` and transformed into `SOJdsa*K3\x07u\x07` (containing two _bell_ chars) which is the actual input to trigger the success message in the program...

### Level 4
This time the program outputs the encrypted flag. Following back XREFs from `printf`, we find the encryption function `FUN_00401453` that we can easily reverse to reveal the flag:
```python
OUT = bytearray(b'Amcm\x17QBu^YP+`lD\x17V1pvY^BdR')
      
def FUN_00401410(n):  # n<=3 or is_prime(n)
    if n>3:
        p = 2
        while p<n:
            if n%p==0:
                return False
            p += 1
    return True

def FUN_00401453(msg):  # (en/de)crypt
    out = bytearray(msg)
    for i in range(len(msg)):
        if not FUN_00401410(i):
            out[i] ^= 0x37
        else:
            out[i] ^= i
    return out

print(FUN_00401453(OUT))
```
That said and done, there was a simpler approach: We could have continued following XREFs to discover `FUN_004014e6`, which calls `FUN_00401453` to encrypt the clear flag `Alan Turing Was a Geniuse` written as local variables in the stack.
