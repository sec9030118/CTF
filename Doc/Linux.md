# Linux

## Reverse shells

* **Cheatsheets:** [PATT](https://github.com/swisskyrepo/PayloadsAllTheThings/blob/master/Methodology%20and%20Resources/Reverse%20Shell%20Cheatsheet.md), [pentestmonkey](http://pentestmonkey.net/cheat-sheet/shells/reverse-shell-cheat-sheet)
* [PHP](https://github.com/pentestmonkey/php-reverse-shell)
* [Upgrading terminals](https://blog.ropnop.com/upgrading-simple-shells-to-fully-interactive-ttys)
* [pwncat](https://github.com/calebstewart/pwncat)

```
revsh$ python3 -c 'import pty; pty.spawn("/bin/bash")'
Ctrl-Z
$ stty raw -echo; fg
```

## Privilege escalation (Priv. Esc.)

* **Methodology & checklists:** [PATT](https://github.com/swisskyrepo/PayloadsAllTheThings/blob/master/Methodology%20and%20Resources/Linux%20-%20Privilege%20Escalation.md), [g0tmi1k](https://blog.g0tmi1k.com/2011/08/basic-linux-privilege-escalation)
* [GTFOBins](https://gtfobins.github.io/)

### Enumeration
* [LinEnum](https://github.com/rebootuser/LinEnum)
* [Linux Smart Enumeration](https://github.com/diego-treitos/linux-smart-enumeration)
* [LinPEAS](https://github.com/carlospolop/privilege-escalation-awesome-scripts-suite)
* **Processes:** [pspy](https://github.com/DominicBreuker/pspy)
* **Exploits:** [Linux Exploit Suggester](https://github.com/The-Z-Labs/linux-exploit-suggester)

## Misc.
* X automation: [`xdotool`](https://github.com/jordansissel/xdotool/blob/master/xdotool.pod) (+ `xwininfo` or `xprop` to get window id)
