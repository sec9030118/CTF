# Cryptography

## Hash cracking

#### Hash crackers
* [John The Ripper (jumbo)](https://github.com/openwall/john)
* [hashcat](https://hashcat.net/wiki/doku.php?id=example_hashes)

#### Helper tools
* [haiti](https://noraj.github.io/haiti/), [Name-That-Hash](https://nth.skerritt.blog/): hash identifiers
* [wordlistctl](https://github.com/BlackArch/wordlistctl): fetch wordlists from online archives
* [TTPassGen](https://github.com/tp7309/TTPassGen): generate/modify wordlists from rules
* [CeWL](https://github.com/digininja/CeWL): generate wordlists from websites
* [lyricpass](https://github.com/initstring/lyricpass): generate wordlists from song lyrics
* [Mentalist](https://github.com/sc0tfree/mentalist)
* [`crunch`](https://sourceforge.net/projects/crunch-wordlist/)
* [`sre_yield`](https://github.com/sre-yield/sre-yield) Python module

#### Online DBs
* <https://crackstation.net/>
* <https://md5hashing.net/search>

#### Specific topics
* MD5 collisions: [hashclash](https://github.com/cr-marcstevens/hashclash), [md5-tunneling](https://github.com/s1fr0/md5-tunneling), [evilize](https://www.mscs.dal.ca/~selinger/md5collision/)

Single-block (512 bits, 64B) MD5 collision from [Marc Stevens](https://marc-stevens.nl/research/md5-1block-collision/):
```
4dc968ff0ee35c209572d4777b721587d36fa7b21bdc56b74a3dc0783e7b9518afbfa200a8284bf36e8e4b55b35f427593d849676da0d1555d8360fb5f07fea2
4dc968ff0ee35c209572d4777b721587d36fa7b21bdc56b74a3dc0783e7b9518afbfa202a8284bf36e8e4b55b35f427593d849676da0d1d55d8360fb5f07fea2
```
or [Xie-Feng](https://eprint.iacr.org/2010/643):
```
0e306561559aa787d00bc6f70bbdfe3404cf03659e704f8534c00ffb659c4c8740cc942feb2da115a3f4155cbb8607497386656d7d1f34a42059d78f5a8dd1ef
0e306561559aa787d00bc6f70bbdfe3404cf03659e744f8534c00ffb659c4c8740cc942feb2da115a3f415dcbb8607497386656d7d1f34a42059d78f5a8dd1ef
```
* State-of-the-Art [Hash collisions](https://github.com/corkami/collisions), [SHA1 PDF collisions](https://github.com/nneonneo/sha1collider)
* Zip cracking: [bkcrack](https://github.com/kimci86/bkcrack) (Biham-Kocher known-plaintext attack)

## Modern practical crypto
#### References
* Awesome [Cryptography](https://github.com/sobolevn/awesome-cryptography) / [Crypto Papers](https://github.com/pFarb/awesome-crypto-papers) lists
* AES (illustrated): [A Stick Figure Guide to AES](http://www.moserware.com/2009/09/stick-figure-guide-to-advanced.html), [Block Breakers](https://www.davidwong.fr/blockbreakers/aes.html)
* See also:
  * my [cryptopals repo.](../Cryptopals/)
  * my [algorithms wiki](https://github.com/blegloannec/CodeProblems/wiki/Mathematics) for [Pohlig–Hellman](https://en.wikipedia.org/wiki/Pohlig%E2%80%93Hellman_algorithm), [Baby-step Giant-step](https://en.wikipedia.org/wiki/Baby-step_giant-step), [Pollard's rho](https://en.wikipedia.org/wiki/Pollard%27s_rho_algorithm_for_logarithms)...

#### Books
* Katz & Lindell, _Introduction to Modern Cryptography_
* Hoffstein, Pipher & Silverman, _An Introduction to Mathematical Cryptography_
* Ferguson, Schneier & Kohno, _Cryptography Engineering_
* Schneier, _Applied Cryptography_
* Silverman, [_The Arithmetic of Elliptic Curves_](https://www.pdmi.ras.ru/~lowdimma/BSD/Silverman-Arithmetic_of_EC.pdf)
* Menezes, van Oorschot & Vanstone, [_Handbook of Applied Cryptography_](https://cacr.uwaterloo.ca/hac/)
* Washington, [_Elliptic curves_](https://people.cs.nctu.edu.tw/~rjchen/ECC2012S/Elliptic%20Curves%20Number%20Theory%20And%20Cryptography%202n.pdf)
* Galbraith, [_Mathematics of Public Key Cryptography_](https://www.math.auckland.ac.nz/~sgal018/crypto-book/crypto-book.html)
* Swenson, [_Modern Cryptanalysis_](https://swenson.io/Modern%20Cryptanalysis%20v1.1%202022-01-23.pdf)
* Boneh-Shoup, [_A Graduate Course in Applied Cryptography_](https://toc.cryptobook.us/)

#### Tools
* Python libs:
  * [PyCryptodome](https://pycryptodome.readthedocs.io/en/latest/src/api.html)
  * [python-ecdsa](https://ecdsa.readthedocs.io/en/latest/index.html), [fastecdsa](https://github.com/AntonKueltz/fastecdsa)
  * [gmpy2](https://gmpy2.readthedocs.io/en/latest/mpz.html#mpz-functions)
  ```python
  def crt(a,p, b,q):
    g,u,v = gmpy2.gcdext(p,q)
    assert g==1
    return (b*u*p+a*v*q)%(p*q)
  ```
  * NumPy + [galois](https://github.com/mhostetter/galois)
  * [SymPy](https://docs.sympy.org/latest/)
  * [cypari2](https://cypari2.readthedocs.io/en/latest/)
* [SageMath](https://www.sagemath.org/)
* Factorization & RSA:
  * [FactorDB](http://factordb.com/)
  * `ecm` ([`gmp-ecm`](https://gitlab.inria.fr/zimmerma/ecm) package), `ecm.factor()` in Sage
  * [cado-nfs](https://gitlab.inria.fr/cado-nfs/cado-nfs)
  * [RsaCtfTool](https://github.com/RsaCtfTool/RsaCtfTool)
  * <https://github.com/mimoo/RSA-and-LLL-attacks>
* `dumpasn1`, `pgpdump`
* [`openssl`](https://www.openssl.org/docs/manmaster/man1/)
* [`gpg`](https://gnupg.org/documentation/manuals/gnupg24/gpg.1.html), [OpenPGP (RFC 4880)](https://www.rfc-editor.org/rfc/rfc4880)
* Other code repositories:
  * Lots of stuff: <https://github.com/jvdsn/crypto-attacks>, <https://github.com/ashutosh1206/Crypton>, <https://github.com/Lyutoon/cryptography>
  * Multivariate Coppersmith: <https://github.com/defund/coppersmith>, <https://github.com/kionactf/coppersmith>
  * Multi-modular linear systems & more: <https://github.com/nneonneo/pwn-stuff/tree/main/math>

## Historical crypto
* [dCode](https://www.dcode.fr/)
* <https://www.boxentriq.com/>
* [CrypTool](https://www.cryptool.org/) & [MysteryTwister](https://mysterytwister.org/)
* [Monoalphabetic substitution cracker](https://quipqiup.com/)
* To find old books/codebooks: <https://openlibrary.org/>
