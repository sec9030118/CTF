# Web

## General
### Requests
* [`curl`](https://daniel.haxx.se/docs/curl-vs-wget.html), `wget`
* Python [Requests](https://requests.readthedocs.io/en/master/)
* `httrack`

### Discovery / Fuzzing
* `gobuster`, `dirb`, [dirsearch](https://github.com/maurosoria/dirsearch), [feroxbuster](https://github.com/epi052/feroxbuster): directories and files discovery
* [`ffuf`](https://github.com/ffuf/ffuf) (fast!), `wfuzz`: Web (HTTP requests) fuzzing
* [OWASP DirBuster](https://tools.kali.org/web-applications/dirbuster) (GUI)

### Web traffic sniffing
* [BurpSuite](https://portswigger.net/burp) (set up as a proxy)

### Automated web server scanning
* `nikto` (CLI)
* [OWASP ZAP](https://www.zaproxy.org/) (GUI)
* [Wappalyzer](https://www.wappalyzer.com/) (browser extension)

## Injections
#### SQLi (SQL injection)
* `sqlmap`
* Payloads: [PATT](https://github.com/swisskyrepo/PayloadsAllTheThings/tree/master/SQL%20Injection), [payloadbox](https://github.com/payloadbox/sql-injection-payload-list)

#### XSS (Cross-Site Scripting)
* Payloads: [PATT](https://github.com/swisskyrepo/PayloadsAllTheThings/tree/master/XSS%20Injection), [payloadbox](https://github.com/payloadbox/xss-payload-list), [xss-payloads](http://www.xss-payloads.com/), [PortSwigger](https://portswigger.net/web-security/cross-site-scripting/cheat-sheet)

#### SSTI (Server-Side Template Injection)
* Payloads: [PATT](https://github.com/swisskyrepo/PayloadsAllTheThings/tree/master/Server%20Side%20Template%20Injection)
* [SSTImap](https://github.com/vladko312/SSTImap): SSTI automated detection and exploit

## LFI/RFI (Local/Remote File Inclusion)
* Payloads: [PATT](https://github.com/swisskyrepo/PayloadsAllTheThings/tree/master/File%20Inclusion)

## Misc.
* [wpscan](https://wpscan.com/wordpress-security-scanner): Wordpress scanner
* `.git` directory → see [_Forensics_](./Forensics.md)
