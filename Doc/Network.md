# Network

## General tools
### TCP/UDP sockets
* `nc`/`netcat`, [`ncat`](https://nmap.org/book/ncat-man.html)
* `socat`
* `netstat`
* `ss`

### Mapping with `nmap`
* [Nmap Script Engine (NSE)](https://nmap.org/book/nse.html)

### Sniffing
* `tcpdump`
* Wireshark (GUI) & [tools](https://wiki.wireshark.org/Tools) (`tshark` CLI, `dumpcap`, ...)
* `dsniff` & tools (`arpspoof`, ...)
* [A-Packets](https://apackets.com): Online PCAP analysis
* see also `iftop`

### Python tools
* [pwntools](https://docs.pwntools.com/en/stable/), [Paramiko](https://docs.paramiko.org/en/stable/) (SSH)
* [Scapy](https://scapy.readthedocs.io/en/latest/)
* [Impacket](https://github.com/fortra/impacket)

## Brute-force
* `hydra`
* `ncrack`
* `medusa`
* [patator](https://github.com/lanjelot/patator)

## Pivoting techniques
* PATT: [Network Pivoting Techniques](https://github.com/swisskyrepo/PayloadsAllTheThings/blob/master/Methodology%20and%20Resources/Network%20Pivoting%20Techniques.md)
* Hacktricks: [Tunneling and Port Forwarding](https://book.hacktricks.xyz/generic-methodologies-and-resources/tunneling-and-port-forwarding)
* [`proxychains`](https://github.com/rofl0r/proxychains-ng)

## SMB/Samba
* `smbclient`/`smbget`
* [enum4linux](https://github.com/CiscoCXSecurity/enum4linux)
