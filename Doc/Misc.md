# Misc.

## Solvers
* [Z3](https://github.com/ericpony/z3py-tutorial)
* [gCol](https://rhydlewis.eu/gcol/) for efficient graph coloring (in particular the `HybridEA` strategy)

## Printers
* [Cheatsheet](http://hacking-printers.net/wiki/index.php/Printer_Security_Testing_Cheat_Sheet)
* [Printer Exploitation Toolkit (PRET)](https://github.com/RUB-NDS/PRET)
