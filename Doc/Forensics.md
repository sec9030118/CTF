# Forensics

## Memory dump analysis
* [Volatility](https://github.com/volatilityfoundation)
* [findaes](https://sourceforge.net/projects/findaes/): find AES keys in memory
* [AESKeyFind / RSAKeyFind / AESFix](https://citp.princeton.edu/our-work/memory/code/)
* [MemProcFS](https://github.com/ufrisk/MemProcFS)

## Disk/volume image analysis & file recovery
* [SleuthKit](https://sleuthkit.org/) ([CLI tools list](http://wiki.sleuthkit.org/index.php?title=TSK_Tool_Overview)) & Autopsy
* [TestDisk & PhotoRec](https://www.cgsecurity.org/wiki/PhotoRec)

## macOS
* `plistutil`: convert binary `.plist` to XML
* [Chainbreaker](https://github.com/n0fate/chainbreaker): decrypt _keychains_

## Malwares
* [VirusTotal](https://www.virustotal.com/)
* [YARA](https://github.com/VirusTotal/yara)
  * [Resources list](https://github.com/InQuest/awesome-yara)
  * ["Official" signature base](https://github.com/Neo23x0/signature-base), [Yara Rules](https://github.com/Yara-Rules/rules)
  * [Valhalla](https://valhalla.nextron-systems.com/) rules search engine
* [Loki](https://github.com/Neo23x0/Loki) scanner
* [ClamAV](https://www.clamav.net/)
* Rootkit
  * [Awesome Linux Rootkits](https://github.com/milabs/awesome-linux-rootkits) list (e.g. [Reptile](https://github.com/f0rb1dd3n/Reptile), [Diamorphine](https://github.com/m0nad/Diamorphine))
  * [chkrootkit](https://www.chkrootkit.org/), [rkhunter](https://sourceforge.net/projects/rkhunter/files/rkhunter/)

## Misc.
* [Git](https://book.hacktricks.xyz/network-services-pentesting/pentesting-web/git)
