# Reverse Engineering & Binary Exploitation (Pwn)

## ASM
### References
* [x86 instructions](http://ref.x86asm.net/), [bis](https://www.felixcloutier.com/x86/), [ter](https://shell-storm.org/x86doc/)
* [x86 calling conventions](https://en.wikipedia.org/wiki/X86_calling_conventions) (for x86-64 System V ABI, the 6th first arguments are passed through registers: RDI, RSI, RDX, RCX, R8, R9)
* Linux system calls: [syscalls.w3challs.com](https://syscalls.w3challs.com/), [syscall.sh](https://syscall.sh/), [syscalls.mebeim.net](https://syscalls.mebeim.net/)

### ELF
* `objdump`, `readelf`
* `nm` (`-D` for shared libs): list symbols
* `ldd` (unsafe) / `readelf -d` (or `objdump -p`) for safer & multi-arch approach
* `strace`, `ltrace`: trace syscalls / library calls
* [`ptrace`](https://repository.root-me.org/Reverse%20Engineering/x86/Unix/FR%20-%20SSTIC%2006%20-%20Playing%20with%20ptrace.pdf) syscall
* `patchelf`
* `dwarfdump`: dump debug info

### Disassembly / Debugging / Decompiling
* `gdb` / `ddd`<br />**+** Pwn extensions: [GEF](https://github.com/hugsy/gef), [pwndbg](https://github.com/pwndbg/pwndbg), [PEDA](https://github.com/longld/peda) (older)
* [Ghidra](https://ghidra-sre.org/)
* [IDA](https://hex-rays.com/ida-free/)
* [Radare2](https://radare.org/) [[cheatsheet](https://scoding.de/uploads/r2_cs.pdf)] / [Cutter](https://cutter.re/)
* Dynamic symbolic analysis: [angr](https://angr.io/), [Triton](https://github.com/jonathansalwan/Triton), [Miasm](https://github.com/cea-sec/miasm)
* Comparator for [compilers](https://godbolt.org/) & [decompilers](https://dogbolt.org/)
* Disassembler lib.: [Capstone](http://www.capstone-engine.org/), [Zydis](https://zydis.re/)

### Script injection
* [Frida](https://frida.re/)

### Emulation
* `qemu`
* [Unicorn](https://github.com/unicorn-engine/unicorn), [Qiling](https://qiling.io/)

### Binary exploitation (_binex_) / Pwn
* `checksec`
* Shellcodes: [shell-storm](https://shell-storm.org/shellcode/), [exploit-db](https://www.exploit-db.com/shellcodes)
* ROP: [ROPgadget](https://github.com/JonathanSalwan/ROPgadget), [Ropper](https://github.com/sashs/Ropper), [ropshell.com](http://ropshell.com/)
* `libc` DBs: [libc.blukat.me](https://libc.blukat.me/), [libc.rip](https://libc.rip/)

## Misc.
* `pmap`: show memory mappings
* Dump a process from memory: `gcore`, [my script](../Scripts/memdump.py), [bash-memory-dump](https://github.com/hajzer/bash-memory-dump)
* [TryItOnline](https://tio.run/): lots of online interpreters

## Bytecode
### Python
* [xdis](https://github.com/rocky/python-xdis/): cross-version disassembler (`pydisasm` command)
* [Decompyle++](https://github.com/zrax/pycdc): disassembler/decompiler
* [uncompyle6](https://github.com/rocky/python-uncompyle6/): decompiler for versions ≤3.8
* [decompile3](https://github.com/rocky/python-decompile3/): (improved) decompiler for versions 3.7-3.8
* [x-python](https://github.com/rocky/x-python): cross-version bytecode interpreter (written in Python)
* [pyinstxtractor-ng](https://github.com/pyinstxtractor/pyinstxtractor-ng) (improved [pyinstxtractor](https://github.com/extremecoders-re/pyinstxtractor)): cross-version [PyInstaller](https://pyinstaller.org/) extractor

### Java
#### JVM decompilers
* [JD-GUI](https://github.com/java-decompiler/jd-gui)
* [CFR](http://www.benf.org/other/cfr/)
* [Procyon](https://github.com/mstrobel/procyon/wiki/Java-Decompiler) and many more...

#### Android Dalvik (APK, Dex)
* [jadx](https://github.com/skylot/jadx): decompiler
* `apktool`: unpacker, disassembler
* [dex2jar](https://github.com/pxb1988/dex2jar): convert to JVM Class (then use a JVM decompiler)

### .NET / C# / Mono
* [ILSpy](https://github.com/icsharpcode/ILSpy): decompiler
* [dnSpy](https://github.com/dnSpy/dnSpy)/[dnSpyEx](https://github.com/dnSpyEx/dnSpy): decompiler, debugger

### Lua
* [unluac](https://sourceforge.net/projects/unluac/): Lua 5.0-5.4 decompiler

### Exotic
#### WebAssembly (WASM)
* [WABT (WebAssembly Binary Toolkit)](https://github.com/WebAssembly/wabt) (package `wabt`): WASM/WAT translation, `wasm-decompile` decompiler
* Chromium browser has a good integrated WASM debugger
* [Cetus](https://github.com/Qwokka/Cetus): Cheat Engine-like browser extension for WASM games
* [Ghidra Wasm plugin](https://github.com/nneonneo/ghidra-wasm-plugin)

#### Flash
* [FFDec](https://github.com/jindrapetrik/jpexs-decompiler/releases): SWF decompiler

#### Ethereum ("Web3")
* [Remix IDE](https://remix.ethereum.org), [web3.py](https://web3py.readthedocs.io/en/stable/) lib.
* [Weaknesses registry](https://swcregistry.io/)
* [Panoramix](https://github.com/palkeo/panoramix): EVM decompiler

#### Godot
* [Godot RE Tools](https://github.com/bruvzg/gdsdecomp/releases): unpacking, recovery, decompiling

#### [Unity](https://github.com/imadr/Unity-game-hacking) (see also C#/Mono above)

#### Gameboy
* Opcodes (~Z80, but not exactly) [[1](http://www.devrs.com/gb/files/opcodes.html), [2](https://www.pastraiser.com/cpu/gameboy/gameboy_opcodes.html)]
* [RGBDS](https://rgbds.gbdev.io/): GB assembler
* [mgbdis](https://github.com/mattcurrie/mgbdis): GB disassembler

#### GBA (ARM7)
* [mGBA](https://mgba.io/): emulator, debugger
* [Ghidra loader](https://github.com/pudii/gba-ghidra-loader)

#### Shaders
* [SPIR-V](https://en.wikipedia.org/wiki/Standard_Portable_Intermediate_Representation) disassembly/decompilation: [SPIRV-Tools](https://github.com/KhronosGroup/SPIRV-Tools)/[SPIRV-Cross](https://github.com/KhronosGroup/SPIRV-Cross) distributed through [Vulkan SDK](https://vulkan.lunarg.com/sdk/home)
