# Home

## Challenges
* [Root Me](https://www.root-me.org/)
* [TryHackMe](https://tryhackme.com/)
* [HackTheBox](https://app.hackthebox.com/)
* **Crypto**
  * [Cryptopals](https://cryptopals.com/)
  * [CryptoHack](https://cryptohack.org/)
* **Pwn**
  * [ROP Emporium](https://ropemporium.com/)
* **Others**
  * [TheBlackSide](https://theblackside.fr/)
  * [Hackropole](https://hackropole.fr/)
  * [picoCTF](https://play.picoctf.org/practice)
  * Lists: [GH1](https://github.com/apsdehal/awesome-ctf#wargames), [GH2](https://github.com/devploit/ctf-awesome-resources#online-platforms), [W3Chall](https://www.wechall.net/active_sites)

### CTF
* **Archives**
  * <https://ctftime.org/>
  * <https://github.com/sajjadium/ctf-archives>
* **Write-ups**
  * <https://github.com/ctfs/>
  * <http://shell-storm.org/repo/CTF/>

## General resources
### Methodology & resources
* [PayloadsAllTheThings](https://github.com/swisskyrepo/PayloadsAllTheThings)
* [HackTricks](https://book.hacktricks.xyz/)
* [Rawsec's Inventory](https://inventory.raw.pm/overview.html)
* [PortSwigger](https://portswigger.net/web-security/all-materials)
* [OWASP Cheat Sheets](https://cheatsheetseries.owasp.org/)
* [Payload Box](https://github.com/payloadbox)
* Others: [pentestmonkey](http://pentestmonkey.net/category/cheat-sheet), <https://cheatsheet.haax.fr>, <https://www.thehacker.recipes>, [Orange Arsenal](https://github.com/Orange-Cyberdefense/arsenal)
* [SecLists](https://github.com/danielmiessler/SecLists):
  * Web discovery: `/Discovery/Web-Content/{common.txt, big.txt}`
  * Passwords: `/Passwords/Leaked-Databases/rockyou.txt`

### [CyberChef](https://gchq.github.io/CyberChef/)

### Python libs
* General: [pwntools](https://docs.pwntools.com/)
* Crypto.: [PyCryptodome](https://pycryptodome.readthedocs.io/en/latest/src/api.html)

### Shell
* [`man sh`](https://linux.die.net/man/1/dash) (dash)
* [Bash Hackers](https://wiki-dev.bash-hackers.org/)
* [explainshell](https://explainshell.com/), [tldr](https://tldr.sh/)
* [Awesome Bash list](https://github.com/awesome-lists/awesome-bash)
* Pure [BASH](https://github.com/dylanaraps/pure-bash-bible) / [SH](https://github.com/dylanaraps/pure-sh-bible) Bible, TLDP [Beginners](https://tldp.org/LDP/Bash-Beginners-Guide/html/index.html) / [Advanced](https://tldp.org/LDP/abs/html/) Bash
* [`shellcheck`](https://www.shellcheck.net/), [`shfmt`](https://github.com/mvdan/sh)

### OS
* Debian-based: [Kali](https://www.kali.org/get-kali/), [Parrot](https://parrotsec.org/download/)
* Privacy-focused: Tails, Whonix, Qubes
