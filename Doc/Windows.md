# Windows

🙄

### MS protocols clients
* [RDP](https://en.wikipedia.org/wiki/Remote_Desktop_Protocol) clients: Remmina, `xfreerdp`
* MS SQL client: `sqsh` (then typically `xp_cmdshell`, if ever available, to execute arbitrary system commands)

### Tools
* [`oledump.py`](https://blog.didierstevens.com/programs/oledump-py/): [OLE files](https://en.wikipedia.org/wiki/Compound_File_Binary_Format) explorer
* [`mimikatz`](https://github.com/gentilkiwi/mimikatz): Extracts passwords hashes
* [Process Dump](https://github.com/glmcdona/Process-Dump): Dump process from memory
* [Process Hacker](https://processhacker.sourceforge.io/) (Win7/8), [System Informer](https://systeminformer.sourceforge.io/) (Win10/11): Advanced task manager (see also [native API headers](https://github.com/winsiderss/systeminformer/tree/master/phnt))

### Priv. Esc. / _Living Off the Land_
* [LOLbin](https://lolbas-project.github.io/)