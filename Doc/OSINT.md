# OSINT

### Account checking
* [Namechk](https://namechk.com/)
* [WhatsMyName](https://whatsmyname.app/)
* [NameCheckup](https://namecheckup.com/)
* [sherlock](https://github.com/sherlock-project/sherlock)

### Reputation
* [VirusTotal](https://www.virustotal.com/) (file)
* [InQuest Labs](https://labs.inquest.net/) (IP/domain, file)
* [Cisco Talos](https://talosintelligence.com/reputation_center) (IP/domain)
* [ipinfo.io](https://ipinfo.io/) (IP)
* [emailrep.io](https://emailrep.io/) (email)

### Leaked data
* [scylla.sh](https://scylla.sh/) (down)
* [have i been pwned?](https://haveibeenpwned.com/)
* [DeHashed](https://dehashed.com/) (non-free)
* [Intelligence X](https://intelx.io/) (non-free)
* [Leak-Lookup](https://leak-lookup.com/) (non-free)
* [Snusbase](https://snusbase.com/) (non-free)

### Devices
* [Shodan](https://www.shodan.io/) [[lib/cli](https://github.com/achillean/shodan-python)]: connected devices search engine
* [wigle.net](https://www.wigle.net/): wifi map

### Others
* Ships: <https://www.marinetraffic.com>, <https://www.vesselfinder.com>
* Planes: <https://www.flightradar24.com>
* Radio (WebSDR): <http://websdr.org>


### Google Dorking [[cheatsheet1](https://gist.github.com/sundowndev/283efaddbcf896ab405488330d1bbc06), [cheatsheet2](https://www.sans.org/posters/google-hacking-and-defense-cheat-sheet/)]

### [Social-Engineer Toolkit (SET)](https://github.com/trustedsec/social-engineer-toolkit)
