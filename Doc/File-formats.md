# File formats

## Data manipulation & detection

### Usual shell commands
* `dd`: binary file cutting
* `file` (`libmagic`): data type detection (magic numbers)
* `hd`/`hexdump`, `xxd`, `od`
* `strings`: strings detection in binary files
* [etc](https://repository.root-me.org/Administration/Unix/Linux/EN%20-%20The%20GNU%20binary%20utils.pdf)...

### Tools
* ghex, [ImHex](https://imhex.werwolv.net/): hex editors
* `binwalk`: binary file decomposition
* [Hachoir](https://github.com/vstinner/hachoir)
* [CyberChef](https://gchq.github.io/CyberChef/)

### Resources
* [Corkami](https://github.com/corkami) / [Ange Albertini](https://github.com/angea): valuable file formats resources & exploits [[pics](https://github.com/corkami/pics/blob/master/binary/README.md)]
* [Hacktricks - File/Data Carving & Recovery Tools](https://book.hacktricks.xyz/generic-methodologies-and-resources/basic-forensic-methodology/partitions-file-systems-carving/file-data-carving-recovery-tools)

## Image files
#### General & Metadata
* `exiftool` (package `libimage-exiftool-perl`), `exiv2`
* `identify` (ImageMagick)
* see also [CyberChef](https://gchq.github.io/CyberChef/)
* **OCR:** `gocr`, `tesseract`
* **Bar/QR codes:** `zbarimg`, [`pyzbar`](https://github.com/NaturalHistoryMuseum/pyzbar)

#### Steganography
* General resources: [@HackTricks](https://book.hacktricks.xyz/crypto-and-stego/stego-tricks), [stego-toolkit](https://github.com/DominicBreuker/stego-toolkit)
* `steghide` (mostly JPEG, BMP & WAV), [`stegseek`](https://github.com/RickdeJager/stegseek) to brute-force
* [Aperi'Solve](https://www.aperisolve.com/)

#### Forensics
* `pngcheck`: PNG inspection (note in particular `-s`/`-x` options to detect/extract PNG in other files)
* `pngfix` (from `libpng-tools`): Check & fix PNG files
* My codes to decompose [PNG chunks](../Scripts/png_chunks.py) & [JPEG segments](../Scripts/jpeg_segments.py)
* [JPEG Error Level Analysis (ELA)](https://fotoforensics.com/)


## PDF
* [`pdf-parser.py`](https://github.com/DidierStevens/DidierStevensSuite/blob/master/pdf-parser.py)
* `pdfinfo`
* [qpdf](https://qpdf.readthedocs.io/en/stable/cli.html) for transformations


## Audio/Video
* Audacity, Sonic Visualizer
* MediaInfo: audio/video technical & meta data (CLI & GUI)
* `sox` (~`convert` for audio)
* Radio (`.iq`): `multimon-ng`, [Inspectrum](https://github.com/miek/inspectrum), [Universal Radio Hacker](https://github.com/jopohl/urh), GNU Radio
* `ffmpeg` (though infamously unusable :confused:)
